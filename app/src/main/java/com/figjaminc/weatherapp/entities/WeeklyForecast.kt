package com.figjaminc.weatherapp.entities

class WeeklyForecast {
    var name: String? = null
    var date: String? = null
    var temperature = 0.0
    var description: String? = null
    var main_description: String? = null
    var day: String? = null
    var longitude: Double = 0.0
    var latitude: Double = 0.0
}