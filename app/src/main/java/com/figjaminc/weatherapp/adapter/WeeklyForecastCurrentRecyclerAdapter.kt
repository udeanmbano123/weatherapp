package com.figjaminc.weatherapp.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.ui.screens.main.MainViewModel
import com.figjaminc.weatherapp.ui.screens.savedmain.SavedMainViewModel
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.location.Constants
import kotlin.collections.ArrayList


class WeeklyForecastCurrentRecyclerAdapter(
    val viewModel: SavedMainViewModel,
    var arrayList: ArrayList<WeeklyForecast>,
    val context: Context

) : RecyclerView.Adapter<WeeklyForecastCurrentRecyclerAdapter.ProductsViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): WeeklyForecastCurrentRecyclerAdapter.ProductsViewHolder {
        var root = LayoutInflater.from(context).inflate(R.layout.main_item_view, parent, false)
        return ProductsViewHolder(root!!)
    }

    init {
    }


    override fun onBindViewHolder(
        holder: WeeklyForecastCurrentRecyclerAdapter.ProductsViewHolder,
        position: Int
    ) {
        holder.bind(arrayList.get(position))
    }

    override fun getItemCount(): Int {
        if (arrayList.size == 0) {
        } else {

        }
        return arrayList.size
    }


    inner class ProductsViewHolder(private val binding: View) : RecyclerView.ViewHolder(binding) {
        private val dayOfWeek: TextView = binding.findViewById<TextView>(R.id.dayOfWeek)
        private val mainTemp: TextView = binding.findViewById<TextView>(R.id.mainTemp)
        private val weatherImage: ImageView = binding.findViewById<ImageView>(R.id.weatherImage)
        private val fullDateLabel: TextView = binding.findViewById<TextView>(R.id.fullDateLabel)
        private val mainHeader: RelativeLayout =
            binding.findViewById<RelativeLayout>(R.id.mainHeader)
        private val currentHeader: LinearLayout =
            binding.findViewById<LinearLayout>(R.id.currentHeader)
        private val currentHeaderLabel: LinearLayout =
            binding.findViewById<LinearLayout>(R.id.currentHeaderLabel)

        fun bind(item: WeeklyForecast) {
            dayOfWeek.text = item.day
            mainTemp.text =
                String.format("%.1f",item.temperature.minus(Constants.kelvinConstant)).toString() + "" + context.resources.getString(R.string.celsius)

            fullDateLabel.text = item.date.toString()
          Log.v("Verbose","Item description "+item.description!!)
            when {
                item.main_description!!.toLowerCase().contains("sun") -> {
                    mainHeader.setBackgroundColor(context.resources.getColor(R.color.sunny))
                    currentHeader.setBackgroundColor(context.resources.getColor(R.color.sunny))
                    currentHeaderLabel.setBackgroundColor(context.resources.getColor(R.color.sunny))

                }
                item.main_description!!.toLowerCase().contains("cloud") -> {
                   mainHeader.setBackgroundColor(context.resources.getColor(R.color.cloudy))
                    currentHeader.setBackgroundColor(context.resources.getColor(R.color.cloudy))
                    currentHeaderLabel.setBackgroundColor(context.resources.getColor(R.color.cloudy))

                }
                item.main_description!!.toLowerCase().contains("rain") -> {
                   mainHeader.setBackgroundColor(context.resources.getColor(R.color.rainy))
                    currentHeader.setBackgroundColor(context.resources.getColor(R.color.rainy))
                    currentHeaderLabel.setBackgroundColor(context.resources.getColor(R.color.rainy))

                }
            }
            when {
                item.description!!.toLowerCase().contains("sun") -> {
                    weatherImage.setImageResource(R.drawable.clear)

                }
                item.description!!.toLowerCase().contains("cloud") -> {
                    weatherImage.setImageResource(R.drawable.partlysunny)

                }
                item.description!!.toLowerCase().contains("rain") -> {
                    weatherImage.setImageResource(R.drawable.rain)

                }
            }
        }

    }


}

