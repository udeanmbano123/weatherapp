package com.figjaminc.weatherapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.ui.screens.favourites.FavouritesViewModel
import com.figjaminc.weatherapp.ui.screens.main.MainViewModel
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.favourites.favourites
import com.weightypremeir.weatherapp.location.Constants
import com.weightypremeir.weatherapp.location.GPSLocationListener
import kotlin.collections.ArrayList


class WeeklyForecastFavouritesRecyclerAdapter(
    val favourites: favourites,
    val viewModel: FavouritesViewModel,
    var arrayList: ArrayList<WeatherCurrent>,
    val context: Context

) : RecyclerView.Adapter<WeeklyForecastFavouritesRecyclerAdapter.ProductsViewHolder>() {
    var copyList: ArrayList<WeatherCurrent>? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): WeeklyForecastFavouritesRecyclerAdapter.ProductsViewHolder {
        var root = LayoutInflater.from(context).inflate(R.layout.favorite_item_view, parent, false)
        return ProductsViewHolder(root!!)
    }

    init {
        this.copyList = java.util.ArrayList()
        this.copyList!!.addAll(arrayList)

    }

    fun filter(text: String) {
        var text = text
        arrayList.clear()
        var filterCounter: Int = 0
        if (text.isEmpty()) {
            arrayList.addAll(copyList!!)

        } else {
            text = text.toLowerCase()
            for (item in copyList!!) {
                try {
                    if (item!!.name!!.toLowerCase()
                            .contains(text) || item!!.loc_country!!.toLowerCase()
                            .contains(text)
                    ) {
                        arrayList.add(item)
                    }

                    filterCounter += 1
                } catch (ex: Exception) {

                }
            }

        }
        notifyDataSetChanged()
    }


    override fun onBindViewHolder(
        holder: WeeklyForecastFavouritesRecyclerAdapter.ProductsViewHolder,
        position: Int
    ) {
        holder.bind(arrayList.get(position))
    }

    override fun getItemCount(): Int {
        if (arrayList.size == 0) {
        } else {

        }
        return arrayList.size
    }


    inner class ProductsViewHolder(private val binding: View) : RecyclerView.ViewHolder(binding) {
       private val mainName: TextView = binding.findViewById<TextView>(R.id.main_name)

        private val mainTemp: TextView = binding.findViewById<TextView>(R.id.main_temp)

        private val cityCountry: TextView = binding.findViewById<TextView>(R.id.cityCountry)

        private val infoButton: ImageButton = binding.findViewById<ImageButton>(R.id.info_button)

        private val goToMap: ImageButton = binding.findViewById<ImageButton>(R.id.go_to_map)

        private val savedWeather: ImageButton = binding.findViewById<ImageButton>(R.id.next_button)

        fun bind(item: WeatherCurrent) {
            mainName.text = item.name
            mainTemp.text =
                String.format("%.1f",item.maintemp!!.minus(Constants.kelvinConstant)).toString() + "" + context.resources.getString(R.string.celsius)
            cityCountry.text= item.loc_city +" , "+item.loc_country

            infoButton.setOnClickListener {
                favourites!!.pref!!.edit().putFloat(
                    "map_latitude",
                    item.lat!!.toFloat()
                ).apply()
                favourites!!.pref!!.edit().putFloat(
                    "map_longitude",
                    item.lon!!.toFloat()
                ).apply()
                favourites!!.pref!!.edit().putLong(
                    "map_id_current",
                    item.id
                ).apply()
                favourites!!.pref!!.edit().putString(
                    "map_name",
                    item.name
                ).apply()
                favourites!!.pref!!.edit().putString(
                    "map_CityCountry",
                    item.loc_city +" , "+item.loc_country
                ).apply()
                favourites!!.findNavController().navigate(R.id.action_Favourites_to_Info)

            }
            goToMap.setOnClickListener {
                favourites!!.pref!!.edit().putFloat(
                    "map_latitude",
                    item.lat!!.toFloat()
                ).apply()
                favourites!!.pref!!.edit().putFloat(
                    "map_longitude",
                    item.lon!!.toFloat()
                ).apply()
                favourites!!.pref!!.edit().putLong(
                    "map_id_current",
                    item.id
                ).apply()
                favourites!!.pref!!.edit().putString(
                    "map_name",
                    item.name
                ).apply()
                favourites!!.pref!!.edit().putString(
                    "map_CityCountry",
                    item.loc_city +" , "+item.loc_country
                ).apply()
                favourites!!.pref!!.edit().putInt(
                    "from_main",
                   0

                ).apply()
                favourites!!.findNavController().navigate(R.id.action_Favourites_to_Map)

            }
            savedWeather.setOnClickListener {
                favourites!!.pref!!.edit().putFloat(
                    "map_latitude",
                    item.lat!!.toFloat()
                ).apply()
                favourites!!.pref!!.edit().putFloat(
                    "map_longitude",
                    item.lon!!.toFloat()
                ).apply()
                favourites!!.pref!!.edit().putLong(
                    "map_id_current",
                    item.id
                ).apply()
                favourites!!.pref!!.edit().putString(
                    "map_name",
                    item.name
                ).apply()
                favourites!!.pref!!.edit().putString(
                    "map_CityCountry",
                    item.loc_city +" , "+item.loc_country
                ).apply()
                favourites!!.findNavController().navigate(R.id.action_Favourites_to_SavedMain)

            }

        }

        }

    }




