package com.figjaminc.weatherapp.ui.screens.favourites

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.figjaminc.weatherapp.entities.SingleLiveEvent
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.room.repositories.WeatherCurrentRepository
import com.figjaminc.weatherapp.room.repositories.WeatherForecastRepository


class FavouritesViewModel(application: Application) : AndroidViewModel(application){
    var weatherCurrentRepository: WeatherCurrentRepository? = null
    var weatherForecastRepository: WeatherForecastRepository? = null
    var weatherCurrentList: LiveData<List<WeatherCurrent?>?>? = null
    var weatherForecastList: LiveData<List<WeeklyForecast?>?>? = null
    var weather_Current_List: List<WeatherCurrent?>? = null
    var currentID: SingleLiveEvent<Long>? = null

   init {
       weatherCurrentRepository = WeatherCurrentRepository(getApplication())
       weatherForecastRepository = WeatherForecastRepository(getApplication())

   }

     fun load() {
        try {
            weatherCurrentList = getWeatherFavourites()

        } catch (ex: Exception) {
            ex.printStackTrace()

        }

    }
    fun getWeatherFavourites(): LiveData<List<WeatherCurrent?>?>? {
          weatherCurrentList = weatherCurrentRepository!!.getWeatherCurrentInModelFavourites()

        return weatherCurrentList
    }
}