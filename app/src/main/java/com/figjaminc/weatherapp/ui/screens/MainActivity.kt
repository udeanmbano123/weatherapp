package com.figjaminc.weatherapp.ui.screens

import android.Manifest
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.weightypremeir.weatherapp.databinding.ActivityMainBinding
import com.weightypremeir.weatherapp.location.CheckForPermissions.Companion.checkForLocationPermissions
import com.weightypremeir.weatherapp.location.Constants
import com.weightypremeir.weatherapp.location.GPSLocationListener
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.location.GPSTracker
import java.util.*


class MainActivity : AppCompatActivity(),
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
     lateinit var binding: ActivityMainBinding
    var mFusedLocationClient: FusedLocationProviderClient? = null
    var location: Location? = null
    var locationrequest: LocationRequest? = null
    var locationListener: GPSLocationListener? = null
    var locationClient: GoogleApiClient? = null
    var gpsTracker: GPSTracker? = null
    lateinit var locationRequested: Any
    private var pref: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        locationRequested = false
        Constants.gpsfaster = false

        pref = application.getSharedPreferences(
            application.resources.getString(R.string.app_name),
            MODE_PRIVATE
        )
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
        //binding.toolbar.visibility= View.GONE
        supportActionBar!!.title = ""
        binding.fab.visibility = View.GONE
        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        setAppLocale("EN_GB")
        // Permission callback
        permissionChecker();

    }

    private fun requestAppPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            val alert=  AlertDialog.Builder(this)
                .setTitle("Permission Needed")
                .setMessage("Permission is needed to access files from your device...")
                .setCancelable(false)
                .setPositiveButton(
                    "OK"
                ) { dialog, which ->
                    val permissions = arrayOf(
                        android.Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.BLUETOOTH,Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    )
                    ActivityCompat.requestPermissions(this, permissions, 2)

                }
            alert.setCancelable(false)
            val dialog = alert.create()
            dialog.setCancelable(false)
            dialog.show()
           dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.black));

        } else {
            val permissions = arrayOf(
                android.Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CAMERA,
                Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.BLUETOOTH,Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
            ActivityCompat.requestPermissions(this, permissions, 2)

        }
    }

    private fun permissionChecker() {
        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

                // Yay, permission was granted
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

                gpsTracker = GPSTracker(this.applicationContext, this,pref!!)
                gpsTracker!!.createGoogleAPI()
                gpsTracker!!.createmfusedinstance()


                locationListener =
                    GPSLocationListener(this)
                starting()
                val res = gpsTracker!!.refreshLocation(true)
            pref!!.edit().putString("appKey",resources.getString(R.string.AppKey))
                .apply()
           /* if(!pref!!.getBoolean("started",false)){
            val intent = intent
            pref!!.edit().putBoolean("started",true)
                .apply()
            finish()
            startActivity(intent)
            }*/
        } else {
            requestAppPermission()
        }
    }

    override fun onRequestPermissionsResult(
        permsRequestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(permsRequestCode, permissions, grantResults)
        if (grantResults.size > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            // now, you have permission go ahead
            permissionChecker()
        } else {
            requestAppPermission()
        }
        return
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    //set locale
    private fun setAppLocale(localeCode: String) {
        try {

            val locale: Locale = Locale(localeCode)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            this!!.applicationContext?.resources?.updateConfiguration(
                config,
                this!!.applicationContext?.resources?.displayMetrics
            )
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
         when (item.itemId) {
            R.id.exit -> {
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
            R.id.favourites -> {
          findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.action_Main_to_Favourites)

            }

             R.id.places -> {
                 findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.action_Main_to_MapsAll)

             }
             R.id.nearbyplaces -> {
                 findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.action_Main_to_NearBy)

             }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun starting() {
        Log.d("debug", "Connecting GoogleApiClient")
        if (!connectHelper()) {
            Log.d("debug", "Failed to connect GoogleAPIClient")
        }
    } //starting().

    private fun connectHelper(): Boolean {
        locationClient!!.connect()
        return if (locationClient!!.isConnected) {
            Log.d("debug", "connected to GoogleAPIClient")
            true
        } else {
            try {
                Thread.sleep(1000)
            } catch (e: java.lang.Exception) {
            }
            if (locationClient!!.isConnected) {
                Log.d("debug", "connected to GoogleAPIClient")
                return true
            }
            false
        } //else.

        return false
    } //connectHelper().

    override fun onConnected(p0: Bundle?) {
        Log.d("debug", "GoogleAPIClient connected.")
    }

    override fun onConnectionSuspended(p0: Int) {
        print("connect failed, cause = $p0")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.d("debug", "Connection Failed, connection result= $p0")
        val apiAvailability = GoogleApiAvailability.getInstance()
        //	apiAvailability.getErrorDialog(current_act, arg0.getErrorCode(), 0);
        //	apiAvailability.getErrorDialog(current_act, arg0.getErrorCode(), 0);
        try {
            p0.startResolutionForResult(
                this,
                p0.getErrorCode()
            )
        } catch (sie: IntentSender.SendIntentException) {
            Log.d(
                "debug",
                "SendIntentException encountered when trying to resolve API connection issue."
            )
        }
    }

    fun disableCloselocation() {
        if (mFusedLocationClient != null) {
            Constants.distance = 0
            Constants.returnfrombackround = 0
            Constants.goingbackround = System.currentTimeMillis()
            if (!pref!!.getBoolean("improve_gps", false)) {
                mFusedLocationClient!!.removeLocationUpdates(GPSLocationListener.mLocationCallback)
            }
            Constants.GEOFENCE_STATUS = 0
            //Constants.Current_store_lat = 0;
            //Constants.Current_store_long = 0;
            Constants.geoconnectonce = true
        }
    } //Udean Mbano to deal with location closing and conserving battery

    fun fakelocationDialog() {
        AlertDialog.Builder(this)
            .setTitle("Fake Location Alert")
            .setMessage("Please uninstall Fake Locations apps now from settings")
            .setPositiveButton(
                "Settings"
            ) { dialog, which ->
                val intent = Intent(Settings.ACTION_APPLICATION_SETTINGS)
                startActivity(intent)
                dialog.dismiss()
            } //onClick().
            .setNegativeButton(
                "Close"
            ) { dialog, which ->
                dialog.dismiss()
                increaseGpsSpeed(false)
            }.setIcon(android.R.drawable.ic_dialog_alert).setCancelable(false).show()

    }

    private fun increaseGpsSpeed(speed: Boolean) {

        if (gpsTracker!!.checkLocationEnabled()) {
            Constants.gpsfaster = speed
            try {
                gpsTracker!!.refreshLocation(false)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

    } // watson

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    // Ask for permission to access device Location.
    fun askPermission(act: AppCompatActivity, perm: Int?) {
        if (act == null) {
            return
        }
        if (perm == 1) {
            ActivityCompat.requestPermissions(
                act,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), perm!!
            )
        } else {
            ActivityCompat.requestPermissions(
                act,
                arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                perm!!
            )
        }

    }//askPermission().

    fun checkPermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED
            ) {
                Log.v("Error", "Permission is granted")
                true
            } else {
                Log.v("Error", "Permission is revoked")
                try {
                    checkForLocationPermissions(this as AppCompatActivity)
                } catch (e: Exception) {
                }
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_PHONE_STATE
                ) == PackageManager.PERMISSION_GRANTED
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("Error", "Permission is granted")
            true
        }
        return (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_PHONE_STATE
        ) == PackageManager.PERMISSION_GRANTED);
    }

    fun hideMenuOptions() {
        try {   binding.toolbar!!.menu!!.findItem(R.id.favourites)!!.setVisible(false)
        binding.toolbar!!.menu!!.findItem(R.id.places)!!.setVisible(false)
        binding.toolbar!!.menu!!.findItem(R.id.nearbyplaces)!!.setVisible(false)
        }catch(e:Exception){

        }
    }
    fun showMenuOptions() {
        try {
            binding.toolbar!!.menu!!.findItem(R.id.favourites)!!.setVisible(true)
            binding.toolbar!!.menu!!.findItem(R.id.places)!!.setVisible(true)
            binding.toolbar!!.menu!!.findItem(R.id.nearbyplaces)!!.setVisible(true)
        }catch(e:Exception){

        }
    }

}