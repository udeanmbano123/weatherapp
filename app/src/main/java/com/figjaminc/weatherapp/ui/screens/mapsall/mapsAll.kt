package com.figjaminc.weatherapp.ui.screens.mapsall

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.location.Address
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.figjaminc.weatherapp.adapter.WeeklyForecastFavouritesRecyclerAdapter
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.databinding.MapsFragmentBinding
import com.google.android.gms.maps.MapView
import com.google.android.gms.common.GooglePlayServicesNotAvailableException

import com.google.android.gms.maps.MapsInitializer

import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.CameraUpdate

import com.google.android.gms.maps.model.Marker




class mapsAll : Fragment() ,OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    var builder = LatLngBounds.Builder()
    var latlng: LatLng? = null
    companion object {
        fun newInstance() = mapsAll()
    }
    var pref: SharedPreferences? = null

    private var _binding: MapsFragmentBinding? = null
    private val mapView: MapView? = null
    private lateinit var viewModel: MapsAllViewModel
    private var googleMap: GoogleMap? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MapsFragmentBinding.inflate(inflater, container, false)
        (context as MainActivity).binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            findNavController().navigate(R.id.action_MapsAll_to_Main)
        })
        _binding!!.mapView.onCreate(savedInstanceState);
        pref = this.requireActivity().application.getSharedPreferences(
            this.requireActivity().application.resources.getString(R.string.app_name),
            AppCompatActivity.MODE_PRIVATE
        )
        viewModel = ViewModelProvider(this).get(MapsAllViewModel::class.java)
        (context as MainActivity).hideMenuOptions()
        initialiseAdapter()
        // Set the map ready callback to receive the GoogleMap object
        return _binding!!.root
    }
    private fun initialiseAdapter() {

        viewModel!!.load()
        observeData()
    }
    private fun observeData() {

        viewModel!!.getWeatherFavourites()?.observe(viewLifecycleOwner,
            Observer<List<WeatherCurrent?>?> {

                if (it?.isNullOrEmpty() == false) {

                    _binding!!.mapView.getMapAsync(this);

                }


            })




    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }
    override fun onMapReady(googleMap: GoogleMap) {
        // Need to call MapsInitializer before doing any CameraUpdateFactory calls
        // Need to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(this.requireActivity())
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }


        mMap = googleMap
        val sydney = LatLng(pref!!.getFloat(
            "map_latitude",
            0F
        ).toDouble(),pref!!.getFloat(
            "map_longitude",
            0F
        ).toDouble())
        var marker= mMap!!.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in "+pref!!.getString(
                    "map_name",
                    ""
                )))
        builder.include(marker!!.position)

        for (w in viewModel!!.weatherCurrentList!!.value!!) {


            latlng = LatLng(w!!.lat!!.toDouble(),w!!.lon!!.toDouble())
            val marker: Marker? = mMap!!.addMarker(
                MarkerOptions().position(latlng!!).title(w!!.name!!.toString())
            )
            builder.include(marker!!.position)
        }

        val bounds = builder.build()
        val padding = 0
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
        mMap!!.animateCamera(cu)

    }
    override fun onResume() {
        _binding!!.mapView.onResume()
        super.onResume()
    }


}