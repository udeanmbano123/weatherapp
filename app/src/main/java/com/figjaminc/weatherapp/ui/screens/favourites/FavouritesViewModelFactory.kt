package com.figjaminc.weatherapp.ui.screens.favourites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import android.app.Application

class FavouritesViewModelFactory() : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavouritesViewModel::class.java)) {
            return FavouritesViewModel(Application()) as T
        }
        throw IllegalArgumentException("UnknownViewModel")
    }

}