package com.weightypremeir.weatherapp.favourites

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.figjaminc.weatherapp.adapter.WeeklyForecastFavouritesRecyclerAdapter
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.figjaminc.weatherapp.ui.screens.favourites.FavouritesViewModel
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.databinding.FavouritesBinding
import android.view.MenuInflater





/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class favourites : Fragment() {

    private var _binding: FavouritesBinding? = null
    private lateinit var favouritesModel: FavouritesViewModel
    var pref: SharedPreferences? = null
    var progressDialog: ProgressDialog? = null
    private var viewManager: LinearLayoutManager? = null
    private lateinit var mainrecycler: RecyclerView
    private lateinit var weeklyRecyclerFavoritesAdapter: WeeklyForecastFavouritesRecyclerAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        pref = this.requireActivity().application.getSharedPreferences(
            this.requireActivity().application.resources.getString(R.string.app_name),
            AppCompatActivity.MODE_PRIVATE
        )

        _binding = FavouritesBinding.inflate(inflater, container, false)
       favouritesModel = ViewModelProvider(this).get(FavouritesViewModel::class.java)  // initialiseAdapter()
        viewManager = LinearLayoutManager(this.context)
        _binding!!.customerEdtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    weeklyRecyclerFavoritesAdapter.filter(s.toString())
                    _binding!!.recycler.adapter?.notifyDataSetChanged()
                } catch (ex: java.lang.Exception) {

                }
            }
        })
        // This callback will only be called when MyFragment is at least Started.
        (context as MainActivity).binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            findNavController().navigate(R.id.action_Favourites_to_Main)
        })
        (context as MainActivity).window.statusBarColor=resources.getColor(R.color.black)
        (context as MainActivity).binding!!.toolbar.setBackgroundColor(resources.getColor(R.color.black))
        (context as MainActivity).hideMenuOptions()
        initialiseAdapter()
        return binding.root

    }


    private fun initialiseAdapter() {
       _binding!!.recycler.layoutManager = viewManager
        _binding!!.recycler.setItemViewCacheSize(100000);
        progressDialog = ProgressDialog(context, R.style.CustomDialog)
        progressDialog!!.setTitle(resources.getString(R.string.weatherupdates))
        progressDialog!!.setMessage(resources.getString(R.string.loading))
        progressDialog!!.show()
        favouritesModel!!.load()
        observeData()
    }
    private fun observeData() {

        favouritesModel!!.getWeatherFavourites()?.observe(viewLifecycleOwner,
            Observer<List<WeatherCurrent?>?> {

          if (it?.isNullOrEmpty() == false) {
              try {
                  if (progressDialog!!.isShowing) {
                      progressDialog!!.dismiss()
                  }
              } catch (ex: java.lang.Exception) {
                  ex.printStackTrace()
                  Log.e("Error", ex.message.toString())
              }
                  try{
                      weeklyRecyclerFavoritesAdapter=WeeklyForecastFavouritesRecyclerAdapter(
                       this, favouritesModel,
                    it as ArrayList<WeatherCurrent>,
                        this.context as Context
                    )
                    _binding!!.recycler.adapter = weeklyRecyclerFavoritesAdapter
                    _binding!!.recycler.adapter?.notifyDataSetChanged()
                } catch (ex: Exception) {
                ex.printStackTrace()
                Log.e("Error", ex.message.toString())
            }
                }


            })




    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       // binding.buttonSecond.setOnClickListener {
        //    findNavController().navigate(R.id.action_Favourites_to_Main)
        //}
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}