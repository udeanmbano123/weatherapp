package com.figjaminc.weatherapp.ui.screens.savedmain

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.figjaminc.weatherapp.adapter.WeeklyForecastCurrentRecyclerAdapter
import com.figjaminc.weatherapp.adapter.WeeklyForecastRecyclerAdapter
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.databinding.SavedmainBinding
import com.weightypremeir.weatherapp.location.Constants
import java.lang.Exception
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class SavedMain : Fragment() {

    private var _binding: SavedmainBinding? = null
    private lateinit var mainModel: SavedMainViewModel
    var pref: SharedPreferences? = null
    private var viewManager: LinearLayoutManager? = null
   private lateinit var weeklyRecyclerAdapter: WeeklyForecastCurrentRecyclerAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    var progressDialog: ProgressDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pref = this.requireActivity().application.getSharedPreferences(
            this.requireActivity().application.resources.getString(R.string.app_name),
            AppCompatActivity.MODE_PRIVATE
        )
        _binding = SavedmainBinding.inflate(inflater, container, false)
        mainModel = ViewModelProvider(this).get(SavedMainViewModel::class.java)  // initialiseAdapter()
        _binding!!.minTemp.text = ""
        _binding!!.currentTemp.text = ""
        _binding!!.maxTemp.text = ""
        _binding!!.currentTempLargeLabel.text = ""
        _binding!!.currentTempLarge.text = ""
        viewManager = LinearLayoutManager(this.context)
        // This callback will only be called when MyFragment is at least Started.
        (context as MainActivity).binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            findNavController().navigate(R.id.action_SavedMain_to_Favourites)
        })
        (context as MainActivity).hideMenuOptions()
        initialiseAdapter()
        return binding.root

    }

    private fun initialiseAdapter() {
        _binding!!.recycler.layoutManager = viewManager
        _binding!!.recycler.setItemViewCacheSize(100000);
        progressDialog = ProgressDialog(context, R.style.CustomDialog)
        progressDialog!!.setTitle(resources.getString(R.string.weatherupdates))
        progressDialog!!.setMessage(resources.getString(R.string.loading))
        progressDialog!!.show()
        mainModel!!.load(pref!!)
        observeData()
    }

    private fun observeData() {

        mainModel.getWeatherCurrent(pref!!)?.observe(viewLifecycleOwner,
            Observer<List<WeatherCurrent?>?> {

                var it = it?.filter { a ->
                    a!!.id==pref!!.getLong(
                        "map_id_current",
                        0
                    )
                }


                if (it?.isNullOrEmpty() == false) {
                    mainModel!!.currentID!!.value=it!!.get(0)!!.id
                    mainModel!!.weather_Current_List=it!!
                    var forecastList=mainModel!!.getWeatherForecasts(it!!.get(0)!!.id)
                    Log.v("Verbose","select  size ${forecastList!!.size.toString()}")
                    _binding!!.txtLastUpdated.text=resources.getString(R.string.lastupdated)+":"+Constants.getEpochTime(it?.get(0)!!.last_updated).toString()
                    _binding!!.stateName.text =
                      it?.get(0)!!.name!!.toString()
                      if(it?.get(0)!!.isFavourite==1){
                          _binding!!.addToFav!!.setImageResource(R.drawable.ic_baseline_star_24)

                      }else{
                          _binding!!.addToFav!!.setImageResource(R.drawable.ic_baseline_star_border_24)

                      }
                    _binding!!.minTemp.text =
                        String.format("%.1f",it?.get(0)!!.main_temp_min!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.currentTemp.text =
                        String.format("%.1f",it?.get(0)!!.maintemp!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.maxTemp.text =
                        String.format("%.1f",it?.get(0)!!.main_temp_max!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.currentTempLargeLabel.text = it?.get(0)!!.weather_description

                    _binding!!.currentTempLarge.text =
                        String.format("%.1f",it?.get(0)!!.maintemp!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.currentTempLargeLabel.text = "Sunny"

                    if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("cloud")) {
                        _binding!!.currentTempLargeLabel.text = "Cloudy"

                        _binding!!.forestImage.setImageResource(R.drawable.forest_cloudy)
                        _binding!!.currentHeader.setBackgroundColor(resources.getColor(R.color.cloudy))
                        _binding!!.currentHeaderLabel.setBackgroundColor(resources.getColor(R.color.cloudy))
                        _binding!!.recycler.setBackgroundColor(resources.getColor(R.color.cloudy))
                        _binding!!.recyclerHeader.setBackgroundColor(resources.getColor(R.color.cloudy))
                        (context as MainActivity).binding!!.toolbar.setBackgroundColor(resources.getColor(R.color.cloudyimage))
                        (context as MainActivity).getTheme().applyStyle(R.style.WeatherAppCloudy,true);
                        (context as MainActivity).window.statusBarColor=resources.getColor(R.color.cloudyimage)

                        // Call setTheme before creation of any(!) View.
                        (context as MainActivity).getTheme().applyStyle(R.style.WeatherAppCloudy,true);

                    } else if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("sun")) {
                        _binding!!.currentTempLargeLabel.text = "Sunny"

                        _binding!!.forestImage.setImageResource(R.drawable.forest_sunny)
                        _binding!!.currentHeader.setBackgroundColor(resources.getColor(R.color.sunny))
                        _binding!!.currentHeaderLabel.setBackgroundColor(resources.getColor(R.color.sunny))
                        _binding!!.recycler.setBackgroundColor(resources.getColor(R.color.sunny))
                        _binding!!.recyclerHeader.setBackgroundColor(resources.getColor(R.color.sunny))
                        (context as MainActivity).binding!!.toolbar.setBackgroundColor(resources.getColor(R.color.sunnyimage))
                        (context as MainActivity).window.statusBarColor=resources.getColor(R.color.sunnyimage)

                        (context as MainActivity).getTheme().applyStyle(R.style.WeatherApp,true);
                    } else if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("rain")) {
                        _binding!!.currentTempLargeLabel.text = "Rainy"

                        _binding!!.forestImage.setImageResource(R.drawable.forest_rainy)
                        _binding!!.currentHeader.setBackgroundColor(resources.getColor(R.color.rainy))
                        _binding!!.currentHeaderLabel.setBackgroundColor(resources.getColor(R.color.rainy))
                        _binding!!.recycler.setBackgroundColor(resources.getColor(R.color.rainy))
                        _binding!!.recyclerHeader.setBackgroundColor(resources.getColor(R.color.rainy))
                        (context as MainActivity).binding!!.toolbar.setBackgroundColor(resources.getColor(R.color.rainyimage))
                        (context as MainActivity).window.statusBarColor=resources.getColor(R.color.rainyimage)

                        (context as MainActivity).getTheme().applyStyle(R.style.WeatherAppRainy,true);
                    } else if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("clear")) {
                        _binding!!.currentTempLargeLabel.text = "Sunny"

                        _binding!!.forestImage.setImageResource(R.drawable.forest_sunny)
                        _binding!!.currentHeader.setBackgroundColor(resources.getColor(R.color.sunny))
                        _binding!!.currentHeaderLabel.setBackgroundColor(resources.getColor(R.color.sunny))
                        _binding!!.recycler.setBackgroundColor(resources.getColor(R.color.sunny))
                        _binding!!.recyclerHeader.setBackgroundColor(resources.getColor(R.color.sunny))
                        (context as MainActivity).binding!!.toolbar.setBackgroundColor(resources.getColor(R.color.sunnyimage))
                        (context as MainActivity).window.statusBarColor=resources.getColor(R.color.sunnyimage)

                        (context as MainActivity).getTheme().applyStyle(R.style.WeatherApp,true);
                    }
                    loadForecast(forecastList)
                }


            })




    }
     fun loadForecast(forecastList:List<WeeklyForecast?>?) {
        if (!forecastList.isNullOrEmpty()) {
            try {
                if (progressDialog!!.isShowing) {
                    progressDialog!!.dismiss()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                Log.e("Error", ex.message.toString())
            }
            try {
                weeklyRecyclerAdapter = WeeklyForecastCurrentRecyclerAdapter(
                    mainModel!!,
                    forecastList as ArrayList<WeeklyForecast>,
                    this.context as Context
                )

                _binding!!.recycler.adapter = weeklyRecyclerAdapter
                _binding!!.recycler.adapter?.notifyDataSetChanged()
            } catch (ex: Exception) {
                ex.printStackTrace()
                Log.e("Error", ex.message.toString())
            }
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //  findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}