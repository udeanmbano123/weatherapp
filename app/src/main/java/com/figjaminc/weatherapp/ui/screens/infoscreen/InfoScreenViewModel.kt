package com.figjaminc.weatherapp.ui.screens.infoscreen

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.figjaminc.weatherapp.entities.SingleLiveEvent
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.room.repositories.WeatherCurrentRepository
import com.figjaminc.weatherapp.room.repositories.WeatherForecastRepository


class InfoScreenViewModel(application: Application) : AndroidViewModel(application) {

    var weatherCurrentRepository: WeatherCurrentRepository? = null
    var weatherForecastRepository: WeatherForecastRepository? = null
    var weatherCurrentList: LiveData<List<WeatherCurrent?>?>? = null
    var weatherForecastList: LiveData<List<WeeklyForecast?>?>? = null
    var weather_Current_List: List<WeatherCurrent?>? = null
    var currentID: SingleLiveEvent<Long>? = null

    init {
        weatherCurrentRepository = WeatherCurrentRepository(getApplication())
        weatherForecastRepository = WeatherForecastRepository(getApplication())
        currentID = SingleLiveEvent<Long>()
    }

    fun load(pref: SharedPreferences) {
        try {
            weatherCurrentList = getWeatherCurrent(pref!!)

        } catch (ex: Exception) {
            ex.printStackTrace()

        }

    }

    fun updateWeatherCurrentFavourite(latitude: Double,longitude: Double,): Int? {
        var it =  weatherCurrentRepository!!.getWeatherCurrentInModels()?.filter { a ->
            a!!.lat == latitude && a.lon == longitude
        }
        var weather = it!![0]!!.isFavourite
        weather = if (weather == 1) {
            0
        } else {
            1
        }
        weatherCurrentRepository!!.updateFavourite(weather,it!![0] as WeatherCurrent)

        return it!![0]!!.isFavourite


    }

    fun getWeatherCurrent(pref: SharedPreferences): LiveData<List<WeatherCurrent?>?>? {
        weatherCurrentList = weatherCurrentRepository!!.getWeatherCurrentInModel()
        if (weatherCurrentList!!.value != null) {
            currentID!!.postValue(weatherCurrentList!!.value!!.get(0)!!.id)
        }
        return weatherCurrentList
    }

    fun getWeatherForecasts(currentID: Long): List<WeeklyForecast?>? {

        return weatherForecastRepository!!.getWeeklyWeatherForecasts(
            currentID
        )


    }


}