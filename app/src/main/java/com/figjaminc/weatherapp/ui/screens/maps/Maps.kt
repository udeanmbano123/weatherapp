package com.figjaminc.weatherapp.ui.screens.maps

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.databinding.MapsFragmentBinding
import com.google.android.gms.maps.MapView
import com.google.android.gms.common.GooglePlayServicesNotAvailableException

import com.google.android.gms.maps.MapsInitializer

import com.google.android.gms.maps.model.LatLngBounds

class Maps : Fragment() ,OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    var builder = LatLngBounds.Builder()

    companion object {
        fun newInstance() = Maps()
    }
    var pref: SharedPreferences? = null

    private var _binding: MapsFragmentBinding? = null
    private val mapView: MapView? = null
    private lateinit var viewModel: MapsViewModel
    private var googleMap: GoogleMap? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MapsFragmentBinding.inflate(inflater, container, false)
        (context as MainActivity).binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
           if( pref!!.getInt(
                "from_main",
                0

            )==1){
               findNavController().navigate(R.id.action_Maps_to_Main)

           }else {
               findNavController().navigate(R.id.action_Maps_to_Favourites)
           }
        })
        _binding!!.mapView.onCreate(savedInstanceState);
        pref = this.requireActivity().application.getSharedPreferences(
            this.requireActivity().application.resources.getString(R.string.app_name),
            AppCompatActivity.MODE_PRIVATE
        )
        (context as MainActivity).hideMenuOptions()
        // Set the map ready callback to receive the GoogleMap object
        _binding!!.mapView.getMapAsync(this);
        return _binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MapsViewModel::class.java)

    }
    override fun onMapReady(googleMap: GoogleMap) {
        // Need to call MapsInitializer before doing any CameraUpdateFactory calls
        // Need to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(this.requireActivity())
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }


        mMap = googleMap



        val sydney = LatLng(pref!!.getFloat(
            "map_latitude",
            0F
        ).toDouble(),pref!!.getFloat(
            "map_longitude",
            0F
        ).toDouble())
       var marker= mMap!!.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in "+pref!!.getString(
                    "map_name",
                   ""
                )))
        builder.include(marker!!.position)

        mMap!!.setMyLocationEnabled(true)

        val bounds: LatLngBounds = builder.build()
        val padding = 0
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
        mMap!!.animateCamera(cu)

    }
    override fun onResume() {
        _binding!!.mapView.onResume()
        super.onResume()
    }


}