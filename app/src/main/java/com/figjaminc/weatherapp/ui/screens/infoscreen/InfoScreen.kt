package com.figjaminc.weatherapp.ui.screens.infoscreen

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.figjaminc.weatherapp.adapter.WeeklyForecastCurrentRecyclerAdapter
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.weightypremeir.weatherapp.R
import com.weightypremeir.weatherapp.databinding.InfoscreenBinding
import com.weightypremeir.weatherapp.location.Constants
import java.lang.Exception
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class InfoScreen : Fragment() {

    private var _binding: InfoscreenBinding? = null
    private lateinit var mainModel: InfoScreenViewModel
    var pref: SharedPreferences? = null
    private var viewManager: LinearLayoutManager? = null
   private lateinit var weeklyRecyclerAdapter: WeeklyForecastCurrentRecyclerAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    var progressDialog: ProgressDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pref = this.requireActivity().application.getSharedPreferences(
            this.requireActivity().application.resources.getString(R.string.app_name),
            AppCompatActivity.MODE_PRIVATE
        )
        _binding = InfoscreenBinding.inflate(inflater, container, false)
        mainModel = ViewModelProvider(this).get(InfoScreenViewModel::class.java)  // initialiseAdapter()
       viewManager = LinearLayoutManager(this.context)
        // This callback will only be called when MyFragment is at least Started.
        (context as MainActivity).binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            findNavController().navigate(R.id.action_Info_to_Favourites)
        })
        (context as MainActivity).window.statusBarColor=resources.getColor(R.color.black)
        (context as MainActivity).binding!!.toolbar.setBackgroundColor(resources.getColor(R.color.black))
        (context as MainActivity).hideMenuOptions()
        initialiseAdapter()
        return binding.root

    }

    private fun initialiseAdapter() {

        observeData()
    }

    private fun observeData() {

        mainModel.getWeatherCurrent(pref!!)?.observe(viewLifecycleOwner,
            Observer<List<WeatherCurrent?>?> {

                var it = it?.filter { a ->
                    a!!.id==pref!!.getLong(
                        "map_id_current",
                        0
                    )
                }


                if (it?.isNullOrEmpty() == false) {
                    mainModel!!.currentID!!.value=it!!.get(0)!!.id
                    mainModel!!.weather_Current_List=it!!
                    var forecastList=mainModel!!.getWeatherForecasts(it!!.get(0)!!.id)
                    Log.v("Verbose","select  size ${forecastList!!.size.toString()}")
                    _binding!!.cityCountry.text=  it?.get(0)!!.name!!.toString()+","+it?.get(0)!!.loc_country!!.toString()
                    _binding!!.locState.text =
                      it?.get(0)!!.loc_state!!.toString()
                    _binding!!.locCity.text =
                        it?.get(0)!!.loc_city!!.toString()
                    _binding!!.locAddress.text =
                        it?.get(0)!!.loc_address!!.toString()
                    _binding!!.locCountry.text =
                        it?.get(0)!!.loc_country!!.toString()

                    _binding!!.mainTempMin.text =
                        String.format("%.1f",it?.get(0)!!.main_temp_min!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.mainTemp.text =
                        String.format("%.1f",it?.get(0)!!.maintemp!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.mainTempMax.text =
                        String.format("%.1f",it?.get(0)!!.main_temp_max!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.mainTempTop.text =
                        String.format("%.1f",it?.get(0)!!.maintemp!!.minus(Constants.kelvinConstant)).toString() + resources.getString(R.string.celsius)
                    _binding!!.mainHumidity.text =
                        it?.get(0)!!.main_humidity!!.toString()
                   _binding!!.mainDesription.text =
                        it?.get(0)!!.weather_description!!.toString()
                    _binding!!.weatherDescription.text =
                        it?.get(0)!!.weather_description!!.toString()
                    _binding!!.weatherMain.text =
                        it?.get(0)!!.weather_main!!.toString()
                    _binding!!.mainFeelsLike.text =
                        String.format("%.1f",it?.get(0)!!.main_feels_like!!).toString()

                    _binding!!.Visibility.text =
                        it?.get(0)!!.visibility!!.toString()
                    _binding!!.windSpeed.text =
                        String.format("%.1f",it?.get(0)!!.wind_speed!!).toString()
                    _binding!!.windDeg.text =
                        it?.get(0)!!.wind_deg!!.toString()

                    _binding!!.sysSunrise.text =
                       it?.get(0)!!.sys_sunrise!!.toString()
                    _binding!!.sysSunset.text =
                       it?.get(0)!!.sys_sunset!!.toString()
                    _binding!!.timeZone.text =
                        it?.get(0)!!.timezone!!.toString()

                    _binding!!.latitude.text =
                        it?.get(0)!!.lat!!.toString()

                    _binding!!.longitude.text =
                        it?.get(0)!!.lon!!.toString()


                    if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("cloud")) {
                        _binding!!.weatherImage.setImageResource(R.drawable.partlysunny)

                    } else if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("sun")) {
                        _binding!!.weatherImage.setImageResource(R.drawable.partlysunny)
                    } else if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("rain")) {
                        _binding!!.weatherImage.setImageResource(R.drawable.rain)
                    } else if (it?.get(0)!!.weather_main!!.toString().toLowerCase().contains("clear")) {
                        _binding!!.weatherImage.setImageResource(R.drawable.clear)
                    }
                }


            })




    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}