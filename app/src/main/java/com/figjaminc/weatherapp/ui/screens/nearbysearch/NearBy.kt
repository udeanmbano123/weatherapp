package com.figjaminc.weatherapp.ui.screens.nearbysearch

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.PlaceLikelihood
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.weightypremeir.weatherapp.databinding.NearbyFragmentBinding
import java.util.*
import android.widget.Toast

import com.figjaminc.weatherapp.ui.screens.MainActivity

import androidx.annotation.NonNull

import com.google.android.gms.tasks.OnFailureListener

import android.R

import com.google.android.gms.maps.SupportMapFragment

import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse

import com.google.android.gms.tasks.OnCompleteListener





class NearBy : Fragment() ,OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    var builder = LatLngBounds.Builder()

    companion object {
        fun newInstance() = NearBy()
    }
    var pref: SharedPreferences? = null

    private var _binding: NearbyFragmentBinding? = null
    private val mapView: MapView? = null
    private lateinit var viewModel: NearByViewModel
    private var googleMap: GoogleMap? = null
    var placesClient: PlacesClient? = null
    var placeFields: List<*> = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS)
    var latLng: LatLng? = null
    var address: String? = null
    private var placeId: String? = null
    var placeLikelihoods: ArrayList<PlaceLikelihood>? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = NearbyFragmentBinding.inflate(inflater, container, false)
        (context as MainActivity).binding.toolbar.setNavigationOnClickListener(View.OnClickListener {


               findNavController().navigate(com.weightypremeir.weatherapp.R.id.action_Nearby_to_Main)

        })
        _binding!!.mapView.onCreate(savedInstanceState);
        pref = this.requireActivity().application.getSharedPreferences(
            this.requireActivity().application.resources.getString(com.weightypremeir.weatherapp.R.string.app_name),
            AppCompatActivity.MODE_PRIVATE
        )
        (context as MainActivity).hideMenuOptions()
        // Initialize Places.
        // Initialize Places.
        Places.initialize(
            this.requireActivity().applicationContext,
            resources.getString(com.weightypremeir.weatherapp.R.string.apikey)
        )

        // Create a new Places client instance.

        // Create a new Places client instance.
        placesClient = Places.createClient(( context as MainActivity))


        //Get the current place and nearby places of your device

        //Get the current place and nearby places of your device
        getCurrentPlace()

        _binding!!.btnNearbyPlaces!!.setOnClickListener { //setting Marker at nearby places
            setMarkerAtNearbyPlaces()
        }
        // Set the map ready callback to receive the GoogleMap object
        //_binding!!.mapView.getMapAsync(this);
        return _binding!!.root
    }
    private fun getCurrentPlace() {
        val request = FindCurrentPlaceRequest.builder(placeFields as MutableList<Place.Field>).build()

        // Call findCurrentPlace and handle the response (first check that the user has granted permission).
        placesClient!!.findCurrentPlace(request)
            .addOnCompleteListener(OnCompleteListener<FindCurrentPlaceResponse?> { task ->
                if (task.isSuccessful) {
                    val response: FindCurrentPlaceResponse = task.getResult()
                    placeLikelihoods = ArrayList<PlaceLikelihood>()
                    placeLikelihoods!!.addAll(response.placeLikelihoods)

                    //response.getPlaceLikelihoods() will return list of PlaceLikelihood
                    //we need to create a custom comparator to sort list by likelihoods
                    placeLikelihoods!!.sortWith(Comparator { obj1, obj2 ->
                        // ## Ascending order
                        obj1.likelihood.compareTo(obj2.likelihood) // To compare string values
                        // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                        // ## Descending order
                        // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                        // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
                    })


                    //After sort ,it will order by ascending , we just reverse it to get first item as nearest place
                    placeLikelihoods!!.reverse()
                    placeId = placeLikelihoods!![0].getPlace().getId()
                    latLng = placeLikelihoods!![0].getPlace().getLatLng()
                    address = placeLikelihoods!![0].getPlace().getAddress()

                    //Removing item of the list at 0 index
                    placeLikelihoods!!.removeAt(0)
                    _binding!!.mapView.getMapAsync(this)
                }
            }).addOnFailureListener { e ->
            Toast.makeText(
                context,
                "Place not found: " + e.message,
                Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun setMarkerAtNearbyPlaces() {
       try {
           if (!placeLikelihoods!!.isNullOrEmpty()) {
               for (place in placeLikelihoods!!) {
                   var plc = place as PlaceLikelihood
                   val markerOptions =
                       MarkerOptions().position(plc.place.latLng).title(plc.place.address)
                   mMap!!.addMarker(markerOptions)
               }
           }
       }catch(ex:Exception){

       }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        //Adding marker in map at current location
        val markerOptions = MarkerOptions().position(latLng!!).title(address)
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng!!))
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng!!, 15f))
        googleMap.addMarker(markerOptions)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NearByViewModel::class.java)

    }
    override fun onResume() {
        _binding!!.mapView.onResume()
        super.onResume()
    }


}