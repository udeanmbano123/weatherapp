package com.weightypremeir.weatherapp.network

import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface WeatherAppApi {
    @GET("data/2.5/weather")
    fun currentRequest(@Query("lat") lat:Double,@Query("lon")lon:Double,@Query("appid") appid:String): Call<ResponseBody>

     @GET("data/2.5/forecast")
    fun forecastRequest(@Query("lat") lat:Double,@Query("lon") lon:Double,@Query("appid") appid:String ): Call<ResponseBody>

}

