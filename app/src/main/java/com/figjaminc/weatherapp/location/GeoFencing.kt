/**
 * Created by figjam on 2017/07/25.
 * Contains methods for creating, monitoring etc for geofences.
 */
package com.weightypremeir.weatherapp.location

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent

class GeoFencing //Constructor().
    : IntentService(TAG) {
    public override fun onHandleIntent(intent: Intent?) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent.errorCode > -1) {
            Log.d(TAG, "GeofencingEvent error " + geofencingEvent.errorCode)
        } else {
            val transaction = geofencingEvent.geofenceTransition
            val geofences = geofencingEvent.triggeringGeofences
            val geofence = geofences[0]
            if (transaction == Geofence.GEOFENCE_TRANSITION_ENTER && geofence.requestId == Constants.GEOFENCE_ID_FJ) {
                Log.d(TAG, "You are inside Store")
                ///Constants.GEOFENCE_STATUS = 1;
            } else {
                Log.d(TAG, "You are outside Store")
                ///Constants.GEOFENCE_STATUS = 0;
            }
        }
    } //watson, goefencing intent service 31 march 2019

    companion object {
        const val TAG = "GeoIntentService"
    }
} //class GeoFencing.
