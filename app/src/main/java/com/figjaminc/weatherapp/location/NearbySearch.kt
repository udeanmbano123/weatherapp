package com.figjaminc.weatherapp.location

import android.content.SharedPreferences
import com.google.maps.model.PlacesSearchResponse
import com.google.maps.GeoApiContext
import com.google.maps.PlacesApi
import com.google.maps.errors.ApiException
import com.google.maps.model.LatLng
import com.google.maps.model.PlaceType
import com.google.maps.model.RankBy
import com.weightypremeir.weatherapp.location.GPSLocationListener
import java.io.IOException

class NearbySearch {

    fun run(pref:SharedPreferences,mapsKey:String?): PlacesSearchResponse {
        var request = PlacesSearchResponse()
        val context = GeoApiContext.Builder()
            .apiKey(mapsKey)
            .build()
        val location = LatLng(-33.8670522, 151.1957362)
        try {
            request = PlacesApi.nearbySearchQuery(context, location)
                    .radius(5000)
                    .rankby(RankBy.PROMINENCE)
                    .keyword("cruise")
                    .language("en")
                    .type(PlaceType.RESTAURANT)
                    .await()
        } catch (e: ApiException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } finally {
            return request
        }
    }
}