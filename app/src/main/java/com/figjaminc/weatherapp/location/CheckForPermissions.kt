package com.weightypremeir.weatherapp.location

import android.Manifest.permission
import android.content.Context
import android.content.pm.PackageManager
import android.telephony.TelephonyManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback
import androidx.core.content.ContextCompat
/*
Created By Udean Mbano for checking permssions
 */
class CheckForPermissions : OnRequestPermissionsResultCallback {
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val requestCodes = requestCode.toString()
        when (requestCodes) {
            permission.READ_PHONE_STATE -> if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) try {
                val telephonyManager = contexts!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                if (ActivityCompat.checkSelfPermission(contexts!!, permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
                val imei = telephonyManager.deviceId
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        private const val MY_PERMISSIONS_REQUEST_READ_LOCATION = 1
        var contexts: AppCompatActivity? = null
        fun checkForLocationPermissions(context: AppCompatActivity?) {
            contexts = context
            if (ContextCompat.checkSelfPermission(context!!, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(contexts!!, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(contexts!!, arrayOf(permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_READ_LOCATION)
            }
        }
    }
}