package com.weightypremeir.weatherapp.location

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.text.TextUtils
import android.util.Log
import android.view.WindowManager.BadTokenException
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.weightypremeir.weatherapp.R
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

class GPSTracker     //constructor().
    (
    private val mContext: Context,
    private val currentAct: MainActivity?,
    private val pref: SharedPreferences
) : Service(), Runnable {
    // flag for GPS status
    var isGPSEnabled = false
    // flag for network status
    var isNetworkEnabled = false
    var canGetLocation = false
    var geoFencing: GeoFencing? = null

    // Declaring a Location Manager
    protected var locationManager: LocationManager? = null
    fun createGoogleAPI() {
        val deviceName = Build.MODEL
        val deviceMan = Build.MANUFACTURER
        if (currentAct!!.locationClient==null) {
            currentAct!!.locationClient = GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(currentAct)
                .addOnConnectionFailedListener(currentAct)
                .addApi(LocationServices.API)
                .build()
        } //if.
    } //createGoolgeAPI().

    fun createmfusedinstance() {
        currentAct!!.mFusedLocationClient =
            LocationServices.getFusedLocationProviderClient(currentAct)
    }

    fun lastKnownLocation() {
        try {
            if (checkPermission()) {
                /* currentAct.mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        currentAct.previous_location = location;
                    }
                });*/
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun checkLocationEnabled(): Boolean {
        //locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        if (currentAct != null) {
            val provider = Settings.Secure.getString(
                currentAct.contentResolver,
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED
            )
            //Rewritten by Udean Mbano- 21/06/2021
            //watson otherwise any location is okay
            if (provider.contains("gps") || provider.contains("network")) {
                return true
            } else {
                //currentAct.location=null;
                Constants.Loc_Accuracy = 0
                showSettingsDialog()
                return false
            }
        }
        return false
    } //checkLocationEnabled().

    //Added by Udean Mbano Method to know if device had high accuracy on it
    fun deviceHasHighAccuracy(): Boolean {
        var locationMode = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(
                    currentAct!!.contentResolver,
                    Settings.Secure.LOCATION_MODE
                )
            } catch (e: SettingNotFoundException) {
                e.printStackTrace()
            }
            return (locationMode != Settings.Secure.LOCATION_MODE_OFF && locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) //check location mode
        } else {
            val locationProviders = Settings.Secure.getString(
                currentAct!!.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED
            )
            return !TextUtils.isEmpty(locationProviders)
        }
    }

    fun refreshLocation(fromBG: Boolean): Boolean {
        val islocationturnedon = checkLocationEnabled()
        if (pref.getInt("force_location", 0) == 1) {
            pref.edit().putBoolean("oneretryonly", true).apply()
        }
        if (pref.getBoolean("oneretryonly", false)) {
            return false
        } else {
            val result = false
            val counter = 0
            var time_since_location_fix: Long = 700000
            if (currentAct!!.location != null) {
                time_since_location_fix = System.currentTimeMillis() - currentAct.location!!.time
            }
            if ((currentAct.location == null) || fromBG || (currentAct!!.location!!.accuracy > 100) || (time_since_location_fix >= (Constants.time_since_fix))) {
                if ((currentAct!!.locationrequest == null) || (currentAct!!.location == null) || (currentAct!!.location!!.latitude <= 0.0)) {
                    restartLocationUpdates(Constants.UPDATE_INTERVAL)
                } //if.
                val location_thread = Thread(this)
                location_thread.isDaemon = true
                location_thread.start()
            } //if.
            if ((currentAct!!.location == null) || (currentAct!!.location!!.latitude == 0.0) || (currentAct!!.location!!.longitude == 0.0)) {
                return false
            }
        }
        lastKnownLocation()
        return true
    } //refreshLocation().

    private fun refreshLocationListener(update_interval: Int) {
        val locationListener = GPSLocationListener((currentAct)!!)
        currentAct!!.locationListener = locationListener
        currentAct!!.locationrequest = LocationRequest.create()
        currentAct!!.locationrequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        if (Constants.gpsfaster as Boolean) {
            currentAct!!.locationrequest!!.interval = Constants.FASTER_INTERVAL.toLong() //1.5 sec
        } else {
            if (pref.getInt("geofencing", 0) == 1) {
                currentAct.locationrequest!!.interval = Constants.GEOFENCE_INTERVAL.toLong() //3 sec
            } else {
                currentAct.locationrequest!!.interval = Constants.FASTEST_INTERVAL.toLong() //30sec
            }
        }
        currentAct!!.locationrequest!!.fastestInterval = Constants.FASTER_INTERVAL.toLong()
        //dont show the google prompt for better experience code will be commented
        Constants.showlocprompt = false
        if (checkPermission()) {
            currentAct.mFusedLocationClient!!.requestLocationUpdates(
                currentAct!!.locationrequest,
                GPSLocationListener!!.mLocationCallback,
                Looper.myLooper()
            )
        }
    } //refreshLocationListener().

    private fun refreshGoogleApiClient(): Boolean {
        currentAct!!.locationClient!!.connect()
        try {
            Thread.sleep(1000)
        } catch (e: Exception) {
        }
        if (!currentAct!!.locationClient!!.isConnected) {
            Log.d("debug", "GoogleAPIClient not connected yet.")
            //	Toast.makeText(currentAct, "Failed to connect GoogleAPIClient for location, please try again.", Toast.LENGTH_SHORT).show();//debug**
            return false
        } //if.
        Log.d("debug", "GoogleAPIClient refresh successful.")
        return true
    } //refreshGoogleApiClient().

    private fun restartLocationUpdates(update_interval: Int) {
        Log.i("debug", class_name + " restartLocationUpdates()")
        refreshLocationListener(update_interval)
    } //restartLocationUpdates().

    //Checks if app has permission to access device location.
   fun checkPermission(): Boolean {
        var location_permission = ContextCompat.checkSelfPermission(
            mContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        if (!location_permission) {
            currentAct!!.askPermission((currentAct), 1)
        }
        location_permission = ContextCompat.checkSelfPermission(
            mContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        Log.d("debug", class_name + " location_permission=" + location_permission) //debug**
        return location_permission //if.
    } //checkPermission().

    var alertDialog: AlertDialog.Builder? = null
    fun showSettingsDialog() {
        //Rewritten by Udean Mbano- 21/06/2021
        if (alertDialog == null) {
            alertDialog = AlertDialog.Builder((currentAct)!!)
            alertDialog!!.setMessage("FigJam requires your location; Please Enable your Location") // Setting Dialog Message
            alertDialog!!.setPositiveButton(
                "Location Settings"
            ) { dialog, which ->
                // On pressing Settings button
                alertDialog = null
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                currentAct!!.startActivity(intent)
                currentAct.locationRequested = false
            } //onClick().

            alertDialog!!.setNegativeButton(
                "Close"
            ) { dialog, which ->
                // On pressing Settings button
                alertDialog = null
                try {
                    //refreshLocation(false);
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                currentAct!!.locationRequested = false
                dialog.dismiss()
            } //onClick().

            alertDialog!!.setCancelable(false)
            // Showing Alert Message
            try {
                alertDialog!!.show()
            } catch (e: BadTokenException) {
                //use a log message
            }
        }
    }

    fun showSettingsAlert() {
        //Rewritten by Udean Mbano- 21/06/2021
        val alertDialog = AlertDialog.Builder(
            (currentAct)!!, R.style.CustomDialog
        )
        alertDialog.setMessage(currentAct.resources.getString(R.string.enablelocationmessage)) // Setting Dialog Message
        alertDialog.setPositiveButton(
            currentAct.resources.getString(R.string.locationsettings),
            DialogInterface.OnClickListener { dialog, which ->
                // On pressing Settings button
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                currentAct.startActivity(intent)
                currentAct.locationRequested = false
            } //onClick().
        )
        alertDialog.setNegativeButton(
            currentAct.resources.getString(R.string.close),
            object : DialogInterface.OnClickListener // On pressing Settings button
            {
                override fun onClick(dialog: DialogInterface, which: Int) {
                    try {
                        //refreshLocation(false);
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    currentAct.locationRequested = false
                    dialog.dismiss()
                } //onClick().
            })
        alertDialog.setCancelable(false)
        // Showing Alert Message
        try {
            //alertDialog.show()
            val dialog =  alertDialog.create()

            dialog.setCancelable(false)
            dialog.show()
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.black));
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.black))
        } catch (e: BadTokenException) {
            //use a log message
        }
    } //showSettingsAlert().

    override fun onBind(intent: Intent): IBinder? {
        // TODO Auto-generated method stub
        return null
    } //onBind().

    /**
     * Function to check if best network provider
     *
     * @return boolean
     */
    fun canGetLocation(cur_act: AppCompatActivity?): Boolean {
        return canGetLocation
    }

    override fun run() {}

    companion object {
        private val class_name = "GPSTracker"
        var location_update_interval = Constants.UPDATE_INTERVAL
    }
} //class GPSTracker.
