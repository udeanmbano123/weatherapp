package com.weightypremeir.weatherapp.location

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.core.content.FileProvider
import com.weightypremeir.weatherapp.BuildConfig
import java.io.File
import java.lang.Exception
import java.util.*

/*
Created By Udean Mbano for holding constants
 */

object Constants {
    lateinit var gpsfaster: Any
    lateinit var distance: Any
    lateinit var mocklocation: Any
    var goingbackround: Long = 0
    var returnfrombackround: Int = 0
    var VAT_INCLUSIVE: Int = 1
    var VAT_EXCLUSIVE: Int = 2
    lateinit var onetryonly: Any
    lateinit var Loc_Accuracy: Any
    lateinit var showlocprompt: Any
    var DBNAME = "vanSalesDb"
    var mCaptureFileUri: FileUri? = null
   var BASE_URL="https://api.openweathermap.org/"

    var geoconnectonce = true
    var isgeoconnected = false
    var GEOFENCE_STATUS = 0
    const val GEOFENCE_ID_FJ = "FJ"
    const val GEOFENCE_REQ_CODE = 0
    const val GEO_DURATION = (1000 * 3600 * 12 // 12 hour. How long the geofence should last for.
            ).toLong()
    var GEOFENCE_RADIUS = 200.0f // in meters

    val GEOFENCE_RADIUS_NM =
        GEOFENCE_RADIUS * 0.000539957f //GEOFENCE_RADIUS converted to Nautical Miles. (Because distance between degrees of latitude = 60NM)

    const val UPDATE_INTERVAL = 1000 * 15 // 4 seconds. Frequency with which location is checked.

    const val FASTEST_INTERVAL =
        1000 * 30 // 30 second. Min Frequency with which location is checked.


    const val FASTER_INTERVAL = 1500 // 30 second. Min Frequency with which location is checked.

    const val GEOFENCE_INTERVAL = 3000 // 30 second. Min Frequency with which location is checked.

    const val time_since_fix =
        5 * 60 * 1000 //5 minutes. If time since last location fix is greater than this then a location check will be forced.


    var Target_Loc_Accuracy = 25f
    var Current_store_lat = 0.0
    var Current_store_long = 0.0
    var kelvinConstant = 273.15
     var   Loc_Lat = 0.0
    var   Loc_long = 0.0
    lateinit var  lastgoodlocation: ArrayList<String>
    //var BASE_URL="https://3-dot-van-sales-sandbox.uc.r.appspot.com/"
    class FileUri {
        var imageUrl: Uri? = null
        var file: File? = null
    }

    fun createImageFile(context: Context?, prefix: String,filename:File,pathname:String): FileUri {
        val fileUri = FileUri()
        var image: File? = filename


        /*try {
            image = File.createTempFile(prefix + System.currentTimeMillis().toString(), ".png", workingDirectory)

        } catch (e: IOException) {
            e.printStackTrace()
        }*/
        val path =pathname //"$workingDirectory/$prefix + ${System.currentTimeMillis().toString()}.png"
        Log.v("Verbose", "My timestamp $path");

        if (image != null) {

            fileUri.file = image

                fileUri.imageUrl = Uri.parse("file:" + image.absolutePath)

        } else if (path != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (context != null) {
                    fileUri.imageUrl = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, File(path))
                }
            }
        }
        return fileUri
    }
    fun isOnline(c: Context?): Boolean {
        var isOnlineflag = false
        if (c != null) {
            try {
                val cm = c.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                if (cm != null) {
                    val netInfo = cm.activeNetworkInfo
                    /* if (netInfo != null && netInfo.isConnectedOrConnecting() && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
                    if(cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                        isOnlineflag=true;
                    }*/if (netInfo != null) {
                        if (netInfo.type == ConnectivityManager.TYPE_WIFI && cm.activeNetworkInfo!!
                                .isConnectedOrConnecting
                        ) {
                            // connected to wifi
                            isOnlineflag = true
                        } else if (netInfo.type == ConnectivityManager.TYPE_MOBILE && cm.activeNetworkInfo!!
                                .isConnectedOrConnecting
                        ) {
                            // connected to mobile data
                            isOnlineflag = true
                        }
                    } else {
                        isOnlineflag = false
                    }
                } else {
                    // not connected to the internet
                    isOnlineflag = false
                }
            } catch (e: Exception) {
                e.printStackTrace()
                //  Log.v("verbose","Checking network" +e);
                //isOnlineflag=isInternetAvailable();
            }
        }
        // Log.v("verbose","online flag" + isOnlineflag);
        return isOnlineflag
    }

    val workingDirectory: File
        get() {
            val directory = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), BuildConfig.APPLICATION_ID)
            if (!directory.exists()) {
                directory.mkdirs()
            }
            return directory
        }
    fun getEpochDate(ts: Long?): String {

        if (ts == null) return ""
        //Get instance of calendar
        val calendar = Calendar.getInstance(Locale.getDefault())
        //get current date from ts
        calendar.timeInMillis = ts
        //return formatted date
        return android.text.format.DateFormat.format("E, dd MMM yyyy", calendar).toString()

    }

    fun getEpochTime(ts: Long?): String {

        if (ts == null) return ""
        //Get instance of calendar
        val calendar = Calendar.getInstance(Locale.getDefault())
        //get current date from ts
        calendar.timeInMillis = ts
        //return formatted date
        return android.text.format.DateFormat.format("HH:mm:ss", calendar)
            .toString()

    }
}