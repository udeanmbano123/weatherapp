/**
 * Created by Jon on 2017/07/24.
 * Email: jon@figjammobile.com
 */
package com.weightypremeir.weatherapp.location

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.location.Address
import android.os.Build
import android.util.Log
import com.figjaminc.weatherapp.room.repositories.WeatherCurrentRepository
import com.figjaminc.weatherapp.room.repositories.WeatherForecastRepository
import com.figjaminc.weatherapp.ui.screens.MainActivity
import com.weightypremeir.weatherapp.R
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import java.util.*
import android.location.Geocoder


//public class GPSLocationListener implements LocationListener //watson
class GPSLocationListener(act: MainActivity) {
    companion object {
        private const val class_name = "GPSLocationListener"
        private lateinit var mContext: MainActivity
        private lateinit var pref: SharedPreferences
        var weatherCurrentRepository: WeatherCurrentRepository? = null

        @JvmField
        var mLocationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                for (location in locationResult.locations) {
                    try {
                        if (Build.VERSION.SDK_INT >= 18) {
                            if (location.isFromMockProvider) {
                                Constants.Loc_Lat = 0.0
                                Constants.Loc_long = 0.0
                                Constants.Loc_Accuracy = 0.0
                                try {
                                    mContext.disableCloselocation()
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                mContext.fakelocationDialog()
                            } else {
                                //  globalContext.location = location;
                                Log.i(
                                    "debug",
                                    class_name + "Location changed lat: " + location.latitude + " long: " + location.longitude + " Accuracy: " + location.accuracy
                                )
                                Constants.Loc_Lat = location.latitude
                                Constants.mocklocation = false
                                Constants.Loc_long = location.longitude
                                Constants.Loc_Accuracy = location.accuracy
                                Constants.distance = 0

                                var geocoder: Geocoder
                                var addresses: List<Address>
                                geocoder = Geocoder(mContext, Locale.getDefault())

                                addresses = geocoder.getFromLocation(
                                    location.latitude,
                                    location.longitude,
                                    1
                                ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                var city: String? = ""
                                var state: String? = ""
                                var country: String? = ""
                                var postalCode: String? = ""
                                var knownName: String? = ""
                                var address: String? = ""
                                try {
                                 address =
                                    addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()


                                try {
                                    city = addresses[0].getLocality().toString()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Error", ex.message.toString())
                                }
                                try {
                                    state = addresses[0].getAdminArea().toString()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Error", ex.message.toString())
                                }
                                try {
                                    country = addresses[0].getCountryName().toString()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Error", ex.message.toString())
                                }
                                try {
                                    postalCode = addresses[0].getPostalCode().toString()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Error", ex.message.toString())
                                }
                                try {
                                    knownName = addresses[0].getFeatureName().toString()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Error", ex.message.toString())
                                }
                                if (postalCode == null) {
                                    postalCode = ""
                                }
                                if (knownName == null) {
                                    knownName = ""
                                }
                                if (country == null) {
                                    country = ""
                                }
                                if (state == null) {
                                    state = ""
                                }
                                if (city == null) {
                                    city = ""
                                }
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Error", ex.message.toString())
                                }
                                if (Constants!!.isOnline(mContext)) {
                                    pref.edit().putFloat(
                                        "location_latitude",
                                        location.latitude.toFloat()
                                    ).apply()
                                    pref.edit().putFloat(
                                        "location_longitude",
                                        location.longitude.toFloat()
                                    ).apply()
                                    weatherCurrentRepository!!.getWeatherCurrent(
                                        pref!!.getFloat("location_longitude", 0F).toDouble(),
                                        pref!!.getFloat("location_latitude", 0F).toDouble(),
                                        pref!!.getString("appKey", "").toString(),
                                        address,
                                        city!!,
                                        state!!,
                                        country!!,
                                        postalCode!!,
                                        knownName!!
                                    )
                                }
                            }
                        } else {
                            Log.i(
                                "debug",
                                class_name + "Location changed lat: " + location.latitude + " long: " + location.longitude + " Accuracy: " + location.accuracy
                            )
                            Constants.Loc_Lat = location.latitude
                            Constants.mocklocation = false
                            Constants.Loc_long = location.longitude
                            Constants.Loc_Accuracy = location.accuracy
                            Constants.distance = 0
                            var geocoder: Geocoder
                            var addresses: List<Address>
                            geocoder = Geocoder(mContext, Locale.getDefault())

                            addresses = geocoder.getFromLocation(
                                location.latitude,
                                location.longitude,
                                1
                            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            var city: String? = ""
                            var state: String? = ""
                            var country: String? = ""
                            var postalCode: String? = ""
                            var knownName: String? = ""
                            var address: String? = ""
                                    try{
                            address=
                                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()


                            try {
                                city = addresses[0].getLocality().toString()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.e("Error", ex.message.toString())
                            }
                            try {
                                state = addresses[0].getAdminArea().toString()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.e("Error", ex.message.toString())
                            }
                            try {
                                country = addresses[0].getCountryName().toString()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.e("Error", ex.message.toString())
                            }
                            try {
                                postalCode = addresses[0].getPostalCode().toString()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.e("Error", ex.message.toString())
                            }
                            try {
                                knownName = addresses[0].getFeatureName().toString()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.e("Error", ex.message.toString())
                            }
                            if (postalCode == null) {
                                postalCode = ""
                            }
                            if (knownName == null) {
                                knownName = ""
                            }
                            if (country == null) {
                                country = ""
                            }
                            if (state == null) {
                                state = ""
                            }
                            if (city == null) {
                                city = ""
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            Log.e("Error", ex.message.toString())
                        }
                        //checks if app is onine
                            if (Constants!!.isOnline(mContext)) {
                                pref.edit().putFloat(
                                    "location_latitude",
                                    location.latitude.toFloat()
                                ).apply()
                                pref.edit().putFloat(
                                    "location_longitude",
                                    location.longitude.toFloat()
                                ).apply()

                                weatherCurrentRepository!!.getWeatherCurrent(
                                    pref!!.getFloat("location_longitude", 0F).toDouble(),
                                    pref!!.getFloat("location_latitude", 0F).toDouble(),
                                    pref!!.getString("appKey", "").toString(),
                                    address,
                                    city!!,
                                    state!!,
                                    country!!,
                                    postalCode!!,
                                    knownName!!
                                )

                            }
                        }
                        if (location.accuracy < 50) { // save last good location
                            Constants.lastgoodlocation = ArrayList()
                            Constants.lastgoodlocation.add(0, location.latitude.toString())
                            Constants.lastgoodlocation.add(1, location.longitude.toString())
                            Constants.lastgoodlocation.add(2, System.currentTimeMillis().toString())
                            Constants.lastgoodlocation.add(3, location.speed.toString())
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    init {
        mContext = act
        pref = mContext.getSharedPreferences(
            mContext.resources.getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
        weatherCurrentRepository =
            WeatherCurrentRepository((mContext!!.applicationContext as Application))

    } //constructor().
} //class GPSLocationListener.
