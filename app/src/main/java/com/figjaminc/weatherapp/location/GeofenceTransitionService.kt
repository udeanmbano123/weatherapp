package com.weightypremeir.weatherapp.location

import android.app.IntentService
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.location.GeofencingEvent
import java.util.*

class GeofenceTransitionService : IntentService(TAG) {
    override fun onHandleIntent(intent: Intent?) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent) // Retrieve the Geofencing intent

        // Handling errors
        if (geofencingEvent.hasError()) {
            val errorMsg = getErrorString(geofencingEvent.errorCode)
            Log.d("debug", "Geofencing error: $errorMsg")
            return
        } //if.
        val geoFenceTransition = geofencingEvent.geofenceTransition // Retrieve GeofenceTrasition
        Log.d("debug", "Geofencing Event occured: $geoFenceTransition") //debug**

        // Check if the transition type
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            val triggeringGeofences =
                geofencingEvent.triggeringGeofences // Get the geofence that were triggered
            val geofenceTransitionDetails = getGeofenceTransitionDetails(
                geoFenceTransition,
                triggeringGeofences
            ) // Create a detail message with Geofences received
            Toast.makeText(this, "Geofence event -> $geoFenceTransition", Toast.LENGTH_LONG).show()
        } //if.
    } //onHandleIntent().

    // Create a detail message with Geofences received
    private fun getGeofenceTransitionDetails(
        geoFenceTransition: Int,
        triggeringGeofences: List<Geofence>
    ): String {
        val triggeringGeofencesList = ArrayList<String?>() // get the ID of each geofence triggered
        for (geofence in triggeringGeofences) {
            triggeringGeofencesList.add(geofence.requestId)
        }
        var status: String? = null
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            status = "Entering "
            Toast.makeText(this, "Entering area", Toast.LENGTH_LONG).show()
            Log.d("debug", "Entering area") //debug**
        } //if.
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            status = "Dwelling"
            Toast.makeText(this, "Been in area a while", Toast.LENGTH_LONG).show()
            Log.d("debug", "Dwelling in area") //debug**
        } //if.
        else if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            status = "Exiting "
            Toast.makeText(this, "Exiting area", Toast.LENGTH_LONG).show()
            Log.d("debug", "Exiting area") //debug**
        } //else if.
        return status + TextUtils.join(", ", triggeringGeofencesList)
    } //getGeofencTransitionDetails()

    companion object {
        private val TAG = GeofenceTransitionService::class.java.simpleName
        const val GEOFENCE_NOTIFICATION_ID = 0

        // Handle errors
        private fun getErrorString(errorCode: Int): String {
            return when (errorCode) {
                GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE -> "GeoFence not available"
                GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES -> "Too many GeoFences"
                GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS -> "Too many pending intents"
                else -> "Unknown error."
            }
        } //getErrorString().
    }
} //class GeofenceTransitionService.
