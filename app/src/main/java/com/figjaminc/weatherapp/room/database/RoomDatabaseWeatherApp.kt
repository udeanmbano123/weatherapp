package com.figjaminc.weatherapp.room.database

import android.content.Context
import androidx.room.*
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.figjaminc.weatherapp.room.dao.WeatherCurrentDao
import com.figjaminc.weatherapp.room.dao.WeatherForecastDao
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.figjaminc.weatherapp.room.entities.WeatherForecast

/**
 * Author- Udean Mbano
 * Database for Weather App
 */

@Database(
    version =1,
    entities = [WeatherCurrent::class, WeatherForecast::class],
        exportSchema = false
)
abstract class RoomDatabaseWeatherApp : RoomDatabase() {
    abstract fun weatherCurrentDao(): WeatherCurrentDao?
    abstract fun weatherForecastDao(): WeatherForecastDao?
    companion object {
        private var INSTANCE: RoomDatabaseWeatherApp? = null
        private val MIGRATION_1_2: Migration = object : Migration(11,12) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Your migration strategy here
          }
        }

        fun getDatabase(context: Context): RoomDatabaseWeatherApp? {

            if (INSTANCE == null) {
                synchronized(RoomDatabaseWeatherApp::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            RoomDatabaseWeatherApp::class.java,
                            "weatherAppDb"
                        ).allowMainThreadQueries().build()
                    }
                }
            }
            return INSTANCE

        }

    }
}
