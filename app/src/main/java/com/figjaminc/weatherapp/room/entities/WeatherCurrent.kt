package com.figjaminc.weatherapp.room.entities

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index


@Entity(tableName = "weather_current",primaryKeys= ["id"]
)
data class WeatherCurrent(
    @ColumnInfo(name = "id")@NonNull  var id : Long=0L,
@ColumnInfo(name = "lon") var lon : Double?=null,
@ColumnInfo(name = "lat") var lat : Double?=null,
@ColumnInfo(name = "weather_id") var weather_id: Int?=null,
@ColumnInfo(name = "weather_main")var weather_main:String?=null,
@ColumnInfo(name = "weather_description")var weather_description:String?=null,
@ColumnInfo(name = "weather_icon") var weathericon:String?=null,
@ColumnInfo(name = "base")var base:String?=null,
@ColumnInfo(name = "main_temp")var maintemp: Double?=null,
@ColumnInfo(name = "main_feels_like")var main_feels_like: Double?=null,
@ColumnInfo(name = "main_temp_min")var main_temp_min: Double?=null,
@ColumnInfo(name = "main_temp_max")var main_temp_max: Double?=null,
@ColumnInfo(name = "main_pressure")var main_pressure: Int?=null,
@ColumnInfo(name = "main_humidity") var main_humidity: Int?=null,
@ColumnInfo(name = "visibility") var visibility: Int?=null,
@ColumnInfo(name = "wind_speed")var wind_speed: Double?=null,
@ColumnInfo(name = "wind_deg")var wind_deg: Int?=null,
@ColumnInfo(name = "clouds_all")var clouds_all: Int?=null,
@ColumnInfo(name = "dt")var dt : Long?=null,
@ColumnInfo(name = "sys_type")var sys_type: Int?=null,
@ColumnInfo(name = "sys_id")var sys_id: Int?=null,
@ColumnInfo(name = "sys_message")var sys_message:String?=null,
@ColumnInfo(name = "sys_country")var sys_country:String?=null,
@ColumnInfo(name = "sys_sunrise")var sys_sunrise: Long?=null,
@ColumnInfo(name = "sys_sunset")var sys_sunset: Long?=null,
@ColumnInfo(name = "time_zone")var timezone: Long?=null,
@ColumnInfo(name = "name")var name:String?=null,
@ColumnInfo(name = "cod")var cod: Int?=null,
@ColumnInfo(name = "last_updated") var last_updated: Long?=null,
@ColumnInfo(name = "loc_city")var loc_city:String?=null,
@ColumnInfo(name = "loc_state")var loc_state:String?=null,
@ColumnInfo(name = "loc_country")var loc_country:String?=null,
@ColumnInfo(name = "loc_postalCode")var loc_postalCode:String?=null,
@ColumnInfo(name = "loc_knownName")var loc_knownName:String?=null,
@ColumnInfo(name = "loc_address")var loc_address:String?=null,
@ColumnInfo(name = "isFavourite")var isFavourite:Int?=null
)
