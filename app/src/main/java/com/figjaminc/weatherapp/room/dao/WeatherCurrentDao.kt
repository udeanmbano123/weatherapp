package com.figjaminc.weatherapp.room.dao

/**
 * Author-Udean Mbano
 * Data access object for Weather Forecast
 */
import androidx.lifecycle.LiveData
import androidx.room.*
import com.figjaminc.weatherapp.room.entities.WeatherCurrent


@Dao
interface WeatherCurrentDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWeatherCurrent(WeatherCurrent: WeatherCurrent?):Long

    @Update
    fun updateWeatherCurrent(WeatherCurrent: WeatherCurrent?)

    @Transaction
    @Query("SELECT * FROM weather_current WHERE name like '%' || :name || '%'")
    fun findWeatherCurrentsByName(name: String?): List<WeatherCurrent?>?

    @Transaction
    @Query("DELETE FROM weather_current WHERE weather_id = :Id")
    fun deleteWeatherCurrents(Id: Int?)

    @Transaction
    @Query("SELECT * FROM weather_current WHERE weather_id  = :Id")
    fun findWeatherCurrentsById(Id: Int?): List<WeatherCurrent?>?

    @Transaction
    @Query("SELECT * FROM weather_current ")
    fun getAllCompanyWeatherCurrent(): LiveData<List<WeatherCurrent?>?>?
    @Transaction
    @Query("SELECT * FROM weather_current where isFavourite=1 ")
    fun getAllCompanyWeatherFavourites(): LiveData<List<WeatherCurrent?>?>?

    @Transaction
    @Query("SELECT * FROM weather_current  ")
    fun getAllCompanyWeatherCurrentAll():List<WeatherCurrent?>?

    @Transaction
    @Query("Update weather_current set isFavourite=:favouriteFlag WHERE id=:currentId")
    fun updateFavourite(favouriteFlag:Int?,currentId:Long?)

    @Transaction
    fun upsertWeatherCurrent(entity: WeatherCurrent?) {
        val id = insertWeatherCurrent(entity)
        if (id == -1L) {
            updateWeatherCurrent(entity)
        }
    }
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWeatherCurrentAll(WeatherCurrent_Address: List<WeatherCurrent?>?)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateWeatherCurrentAll(WeatherCurrent_Address: List<WeatherCurrent?>?)
    @Transaction
    fun upsertWeatherCurrentAll(WeatherCurrent: List<WeatherCurrent?>?) {
        insertWeatherCurrentAll(WeatherCurrent)
        updateWeatherCurrentAll(WeatherCurrent)
    }
}