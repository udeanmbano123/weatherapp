package com.figjaminc.weatherapp.room.dao

/**
 * Author-Udean Mbano
 * Data access object for Weather Forecast
 */
import androidx.lifecycle.LiveData
import androidx.room.*
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.room.entities.WeatherForecast


@Dao
interface WeatherForecastDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWeatherForecast(WeatherForecast: WeatherForecast?):Long

    @Update
    fun updateWeatherForecast(WeatherForecast: WeatherForecast?)

    @Transaction
    @Query("SELECT * FROM weather_forecast WHERE name like '%' || :name || '%'")
    fun findWeatherForecastsByName(name: String?): List<WeatherForecast?>?

    @Transaction
    @Query("DELETE FROM weather_forecast WHERE currentId=:id")
    fun deleteWeatherForecasts(id:Long?)

    @Transaction
    @Query("SELECT * FROM weather_forecast WHERE weather_id  = :Id")
    fun findWeatherForecastsById(Id: Int?): List<WeatherForecast?>?

    @Transaction
    @Query("SELECT * FROM weather_forecast where lat=:latitude  and lon=:longitude ")
    fun getAllCompanyWeatherForecast(longitude: Double?,latitude: Double?): LiveData<List<WeatherForecast?>?>?

    @Transaction
    @Query("select name, max(Date(dt_txt)) as date, (select main_temp from weather_forecast where lon=t.lon and lat=t.lat and Date(dt_txt)=Date(t.dt_txt)  order by dt desc limit 1) as temperature,(select weather_main from weather_forecast where lon=t.lon and lat=t.lat and Date(dt_txt)=Date(t.dt_txt)  order by dt desc limit 1) as description,case when strftime('%w',Date(t.dt_txt))=='1' then 'Monday' when strftime('%w',Date(t.dt_txt))=='2' then 'Tuesday' when strftime('%w',Date(t.dt_txt))=='3' then 'Wednsday'  when strftime('%w',Date(t.dt_txt))=='4' then 'Thursday'  when strftime('%w',Date(t.dt_txt))=='5' then 'Friday'  when strftime('%w',Date(t.dt_txt))=='6' then 'Saturday' when strftime('%w',Date(t.dt_txt))=='7' then 'Sunday' when strftime('%w',Date(t.dt_txt))=='0' then 'Sunday' else '' end as day , lon as 'longitude',lat as 'latitude' from weather_forecast t where t.currentId=:currentId group by name, Date(dt_txt)")
    fun getWeeklyWeatherForecast(currentId:Long?): LiveData<List<WeeklyForecast?>?>?
    @Transaction
    @Query("select name, max(Date(dt_txt)) as date, (select main_temp from weather_forecast where lon=t.lon and lat=t.lat and Date(dt_txt)=Date(t.dt_txt)  order by dt desc limit 1) as temperature,(select weather_main from weather_forecast where lon=t.lon and lat=t.lat and Date(dt_txt)=Date(t.dt_txt)  order by dt desc limit 1) as description,(select weather_main from weather_current where id=t.currentId  limit 1) as main_description,case when strftime('%w',Date(t.dt_txt))=='1' then 'Monday' when strftime('%w',Date(t.dt_txt))=='2' then 'Tuesday' when strftime('%w',Date(t.dt_txt))=='3' then 'Wednsday'  when strftime('%w',Date(t.dt_txt))=='4' then 'Thursday'  when strftime('%w',Date(t.dt_txt))=='5' then 'Friday'  when strftime('%w',Date(t.dt_txt))=='6' then 'Saturday' when strftime('%w',Date(t.dt_txt))=='7' then 'Sunday' when strftime('%w',Date(t.dt_txt))=='0' then 'Sunday' else '' end as day , lon as 'longitude',lat as 'latitude' from weather_forecast t where t.currentId=:currentId group by name, Date(dt_txt) limit 5")
    fun getWeeklyWeatherForecasts(currentId:Long?): List<WeeklyForecast?>?

    @Transaction
    fun upsertWeatherForecast(entity: WeatherForecast?) {
        val id = insertWeatherForecast(entity)
        if (id == -1L) {
            updateWeatherForecast(entity)
        }
    }
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWeatherForecastAll(WeatherForecast_Address: List<WeatherForecast?>?)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateWeatherForecastAll(WeatherForecast_Address: List<WeatherForecast?>?)
    @Transaction
    fun upsertWeatherForecastAll(WeatherForecast: List<WeatherForecast?>?) {
        insertWeatherForecastAll(WeatherForecast)
        updateWeatherForecastAll(WeatherForecast)
    }
}