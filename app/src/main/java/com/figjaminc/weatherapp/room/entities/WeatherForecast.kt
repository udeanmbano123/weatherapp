package com.figjaminc.weatherapp.room.entities

import androidx.annotation.NonNull
import androidx.room.*
import com.google.type.DateTime

@Entity(tableName = "weather_forecast",foreignKeys = [ForeignKey(
    entity = WeatherCurrent::class,
    onUpdate = ForeignKey.CASCADE,
    parentColumns = ["id"],
    childColumns = ["currentId"]
)],indices = [
    Index("currentId")
]
)
data class WeatherForecast (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Long? = null,
    @ColumnInfo(name = "lon") var lon : Double?=null,
    @ColumnInfo(name = "lat") var lat : Double?=null,
    @ColumnInfo(name = "weather_id") var weather_id: Int?=null,
    @ColumnInfo(name = "weather_main") var weather_main:String?=null,
    @ColumnInfo(name = "weather_description")var weather_description:String?=null,
    @ColumnInfo(name = "weather_icon") var weather_icon:String?=null,
    @ColumnInfo(name = "base")var base:String?=null,
    @ColumnInfo(name = "main_temp")var maintemp: Double?=null,
    @ColumnInfo(name = "main_feels_like")var main_feels_like: Double?=null,
    @ColumnInfo(name = "main_temp_min")var main_temp_min: Double?=null,
    @ColumnInfo(name = "main_temp_max")var main_temp_max: Double?=null,
    @ColumnInfo(name = "main_pressure")var main_pressure: Int?=null,
    @ColumnInfo(name = "main_humidity") var main_humidity: Int?=null,
    @ColumnInfo(name = "main_sea_level") var main_sea_level: Int?=null,
    @ColumnInfo(name = "main_grnd_level") var main_grnd_level: Int?=null,
    @ColumnInfo(name = "main_temp_kf") var  main_temp_kf: Double?=null,
    @ColumnInfo(name = "visibility") var visibility: Int?=null,
    @ColumnInfo(name = "wind_speed") var wind_speed: Double?=null,
    @ColumnInfo(name = "wind_deg") var wind_deg: Int?=null,
    @ColumnInfo(name = "clouds_all") var clouds_all: Int?=null,
    @ColumnInfo(name = "dt") var dt : Long?=null,
    @ColumnInfo(name = "time_zone") var timezone: Long?=null,
    @ColumnInfo(name = "currentId") var currentId : Long?=null,
    @ColumnInfo(name = "name") var name:String?=null,
    @ColumnInfo(name = "cod") var cod: Int?=null,
    @ColumnInfo(name = "dt_txt") var dt_txt:String?=null,
    @ColumnInfo(name = "last_updated")var last_updated: Long?=null,
    @ColumnInfo(name = "loc_city")var loc_city:String?=null,
    @ColumnInfo(name = "loc_state")var loc_state:String?=null,
    @ColumnInfo(name = "loc_country")var loc_country:String?=null,
    @ColumnInfo(name = "loc_postalCode")var loc_postalCode:String?=null,
    @ColumnInfo(name = "loc_knownName")var loc_knownName:String?=null,
    @ColumnInfo(name = "loc_address")var loc_address:String?=null
    )