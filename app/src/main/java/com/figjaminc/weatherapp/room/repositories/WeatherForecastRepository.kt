package com.figjaminc.weatherapp.room.repositories
/**
 * Author-Udean Mbano
 * Repository for Weather Current
 */
import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.figjaminc.weatherapp.entities.WeeklyForecast
import com.figjaminc.weatherapp.room.dao.WeatherForecastDao
import com.figjaminc.weatherapp.room.database.RoomDatabaseWeatherApp
import com.figjaminc.weatherapp.room.entities.WeatherForecast
import com.google.gson.Gson
import com.google.type.DateTime
import com.weightypremeir.weatherapp.network.WeatherAppApi
import com.weightypremeir.weatherapp.network.WeatherAppClient
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.time.LocalDateTime
import java.time.LocalDateTime.parse
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.ofPattern
import java.util.*


class WeatherForecastRepository(application: Application?) {
    private val searchResults = MutableLiveData<List<WeatherForecast>>()
    private var WeatherForecastDao: WeatherForecastDao? = null
    private var allWeatherForecasts: List<WeatherForecast?>? = null
    private fun asyncFinished(results: List<WeatherForecast?>?) {
        searchResults.value = results as List<WeatherForecast>?
    }


    private  class InsertAsyncTask internal constructor(private val asyncTaskDao: WeatherForecastDao) : AsyncTask<WeatherForecast?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: WeatherForecast?): Void? {
            asyncTaskDao.upsertWeatherForecast(params[0])
            return null
        }
    }
    private  class UpdateAsyncTask internal constructor(private val asyncTaskDao: WeatherForecastDao) : AsyncTask<WeatherForecast?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: WeatherForecast?): Void? {
            asyncTaskDao.updateWeatherForecast(params[0])
            return null
        }
    }



    fun insertWeatherForecast(newWeatherForecast: WeatherForecast?) {
        WeatherForecastDao!!.insertWeatherForecast(newWeatherForecast)
    }
    fun insertWeatherForecast(newWeatherForecast: List<WeatherForecast?>?) {
        WeatherForecastDao!!.upsertWeatherForecastAll(newWeatherForecast)
    }
    fun updateWeatherForecast(newWeatherForecast: WeatherForecast?) {
        WeatherForecastDao!!.updateWeatherForecast(newWeatherForecast)

    }

    fun findWeatherForecastByName(name: String?):List<WeatherForecast?>? {
        return WeatherForecastDao!!.findWeatherForecastsByName(name)
    }
    fun getWeatherForecastLive(currentId:Long,longitude: Double?,latitude: Double?,passKey:String?,address: String?,city: String?, state: String?
                               ,country: String?,postalCode: String?,knownName: String?) {
        var weather_forecast_list = ArrayList<WeatherForecast?>()
        if(longitude!=0.0){
            try{
            val call: Call<ResponseBody>? =
                WeatherAppClient.client.create(WeatherAppApi::class.java).forecastRequest(
                    latitude!!, longitude!!,passKey!!
                )
            call!!.enqueue(object : Callback<ResponseBody?> {
                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: retrofit2.Response<ResponseBody?>
                ) {
                    val responseBodyString = response.body()!!.string()


                    if (response!!.body().toString().replace("", "") != "") {
                        try {
                        var json = Gson().toJson(responseBodyString) as String
                        json = json.replace("\\\"", "'")
                        json = json.substring(1, json.length - 1)

                            val jsonObject = JSONObject(json)
                            val jsonArray = jsonObject.getJSONArray("list")
                            for (jsonIndex in 0 until jsonArray.length()) {
                             var weatherForecast = WeatherForecast()
                                weatherForecast!!.lon = longitude
                                weatherForecast!!.lat = latitude
                                weatherForecast!!.weather_id = jsonArray.getJSONObject(jsonIndex).optJSONArray("weather")
                                    .getJSONObject(0).optInt("id", 0)
                                weatherForecast!!.weather_main = jsonArray.getJSONObject(jsonIndex).optJSONArray("weather")
                                    .getJSONObject(0).optString("main", "")
                                weatherForecast!!.weather_description =
                                    jsonArray.getJSONObject(jsonIndex).optJSONArray("weather")
                                        .getJSONObject(0).optString("description", "")
                                weatherForecast!!.weather_icon = jsonArray.getJSONObject(jsonIndex).optJSONArray("weather")
                                    .getJSONObject(0).optString("icon", "")
                                weatherForecast!!.base = ""
                                weatherForecast!!.maintemp = jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optDouble("temp", 0.0)
                                weatherForecast!!.main_feels_like = jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optDouble("feels_like", 0.0)
                                weatherForecast!!.main_temp_min = jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optDouble("temp_min", 0.0)
                                weatherForecast!!.main_temp_max = jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optDouble("temp_max", 0.0)
                                weatherForecast!!.main_pressure = jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optInt("pressure", 0)
                                weatherForecast!!.main_humidity = jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optInt("humidity", 0)
                                weatherForecast!!.main_sea_level= jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optInt("sea_level", 0)
                                weatherForecast!!.main_grnd_level= jsonArray.getJSONObject(jsonIndex).optJSONObject("main")
                                    .optInt("grnd_level", 0)
                                weatherForecast!!.visibility = jsonArray.getJSONObject(jsonIndex).optInt("visibility", 0)
                            weatherForecast!!.wind_speed = jsonArray.getJSONObject(jsonIndex).optJSONObject("wind")
                                    .optDouble("speed", 0.0)
                                weatherForecast!!.wind_deg = jsonArray.getJSONObject(jsonIndex).optJSONObject("wind")
                                    .optInt("deg", 0)
                                weatherForecast!!.clouds_all = jsonArray.getJSONObject(jsonIndex).optJSONObject("clouds")
                                    .optInt("all", 0)
                                weatherForecast!!.dt = jsonArray.getJSONObject(jsonIndex).optLong("dt", 0)
                              //  weatherForecast!!.id = jsonArray.getJSONObject(jsonIndex).optLong("dt", 0)
                                weatherForecast!!.dt_txt =jsonArray.getJSONObject(jsonIndex).optString("dt_txt", "")
                                weatherForecast!!.timezone = jsonObject.optJSONObject("city").optLong("timezone", 0)
                                weatherForecast!!.currentId = jsonObject.optJSONObject("city").optLong("id", 0)
                                weatherForecast!!.name = jsonObject.optJSONObject("city").optString("name", "")
                                weatherForecast!!.cod = jsonObject.optJSONObject("city").optInt("cod", 0)
                                weatherForecast.last_updated = getDateTime()
                                weatherForecast.loc_address=address
                                weatherForecast.loc_city=city
                                weatherForecast.loc_state=state
                                weatherForecast.loc_country=country
                                weatherForecast.loc_postalCode=postalCode
                                weatherForecast.loc_knownName=knownName
                                weatherForecast!!.currentId=currentId
                                weather_forecast_list.add(weatherForecast)
                            }

                            if(!weather_forecast_list.isNullOrEmpty()){
                                WeatherForecastDao!!.deleteWeatherForecasts(currentId)
                                WeatherForecastDao!!.upsertWeatherForecastAll(weather_forecast_list)
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()

                        }

                    }
                }

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    Log.v("Verbose", "consolidatedRequest response is ${t.toString()}")
                }

            })
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e("Error", ex.message.toString())
        }
        }


    }

    fun getSearchResults(): MutableLiveData<List<WeatherForecast>> {
        return searchResults
    }
    fun getWeatherForecast(longitude:Double?,latitude: Double?,passKey:String?): LiveData<List<WeatherForecast?>?>? {
        return WeatherForecastDao!!.getAllCompanyWeatherForecast(longitude,latitude)
    }
    fun getWeeklyWeatherForecast(currentId:Long?): LiveData<List<WeeklyForecast?>?>? {
        return WeatherForecastDao!!.getWeeklyWeatherForecast(currentId)
    }
    fun getWeeklyWeatherForecasts(currentId:Long?): List<WeeklyForecast?>? {
        Log.v("Verbose","select name, max(Date(dt_txt)) as date, (select main_temp from weather_forecast where lon=t.lon and lat=t.lat and Date(dt_txt)=Date(t.dt_txt)  order by dt desc limit 1) as temperature,(select weather_main from weather_forecast where lon=t.lon and lat=t.lat and Date(dt_txt)=Date(t.dt_txt)  order by dt desc limit 1) as description,case when strftime('%w',Date(t.dt_txt))=='1' then 'Monday' when strftime('%w',Date(t.dt_txt))=='2' then 'Tuesday' when strftime('%w',Date(t.dt_txt))=='3' then 'Wednsday'  when strftime('%w',Date(t.dt_txt))=='4' then 'Thursday'  when strftime('%w',Date(t.dt_txt))=='5' then 'Friday'  when strftime('%w',Date(t.dt_txt))=='6' then 'Saturday' when strftime('%w',Date(t.dt_txt))=='7' then 'Sunday' when strftime('%w',Date(t.dt_txt))=='0' then 'Sunday' else '' end as day , lon as 'longitude',lat as 'latitude' from weather_forecast t where t.currentId=$currentId group by name, Date(dt_txt)",)
        return WeatherForecastDao!!.getWeeklyWeatherForecasts(currentId)
    }
    fun getDateTime(): Long {
        val calendar = Calendar.getInstance()
        return calendar.timeInMillis

    }
    init {
        val db: RoomDatabaseWeatherApp? = RoomDatabaseWeatherApp.getDatabase(application!!)
        WeatherForecastDao = db!!.weatherForecastDao()

    }
}