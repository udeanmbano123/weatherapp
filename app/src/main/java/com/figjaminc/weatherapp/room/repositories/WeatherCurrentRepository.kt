package com.figjaminc.weatherapp.room.repositories

/**
 * Author-Udean Mbano
 * Repository for Weather Current
 */
import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.ColumnInfo
import com.figjaminc.weatherapp.room.dao.WeatherCurrentDao
import com.figjaminc.weatherapp.room.database.RoomDatabaseWeatherApp
import com.figjaminc.weatherapp.room.entities.WeatherCurrent
import com.google.gson.Gson
import com.weightypremeir.weatherapp.location.GPSLocationListener
import com.weightypremeir.weatherapp.network.WeatherAppApi
import com.weightypremeir.weatherapp.network.WeatherAppClient
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.util.*


class WeatherCurrentRepository(application: Application?) {
    private val searchResults = MutableLiveData<List<WeatherCurrent>>()
    private var WeatherCurrentDao: WeatherCurrentDao? = null
    private var allWeatherCurrents: List<WeatherCurrent?>? = null
    var weatherForecastRepository: WeatherForecastRepository? = null

    private fun asyncFinished(results: List<WeatherCurrent?>?) {
        searchResults.value = results as List<WeatherCurrent>?
    }


    private class InsertAsyncTask internal constructor(private val asyncTaskDao: WeatherCurrentDao) :
        AsyncTask<WeatherCurrent?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: WeatherCurrent?): Void? {
            asyncTaskDao.upsertWeatherCurrent(params[0])
            return null
        }
    }

    private class UpdateAsyncTask internal constructor(private val asyncTaskDao: WeatherCurrentDao) :
        AsyncTask<WeatherCurrent?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: WeatherCurrent?): Void? {
            asyncTaskDao.updateWeatherCurrent(params[0])
            return null
        }
    }

    private class DeleteAsyncTask internal constructor(private val asyncTaskDao: WeatherCurrentDao) :
        AsyncTask<Int?, Void?, Void?>() {
        protected override fun doInBackground(vararg params: Int?): Void? {
            asyncTaskDao.deleteWeatherCurrents(params[0])
            return null
        }
    }

    fun insertWeatherCurrent(newWeatherCurrent: WeatherCurrent?) {
        WeatherCurrentDao!!.insertWeatherCurrent(newWeatherCurrent)
    }

    fun insertWeatherCurrent(newWeatherCurrent: List<WeatherCurrent?>?) {
        WeatherCurrentDao!!.upsertWeatherCurrentAll(newWeatherCurrent)
    }

    fun updateWeatherCurrent(newWeatherCurrent: WeatherCurrent?) {
        WeatherCurrentDao!!.updateWeatherCurrent(newWeatherCurrent)

    }

    fun deleteWeatherCurrent(Id: Int?) {
        val task = DeleteAsyncTask(WeatherCurrentDao!!)
        task.execute(Id)
    }

    fun findWeatherCurrentByName(name: String?): List<WeatherCurrent?>? {
        return WeatherCurrentDao!!.findWeatherCurrentsByName(name)
    }


    fun getSearchResults(): MutableLiveData<List<WeatherCurrent>> {
        return searchResults
    }

    fun getWeatherCurrent(
        longitude: Double?,
        latitude: Double?,
        passKey: String?,
        address: String?,
        city: String?,
        state: String?,
        country: String?,
        postalCode: String?,
        knownName: String?
    ) {

        var weather_current_list = ArrayList<WeatherCurrent?>()
        if (longitude != 0.0) {
            //check if is favourite set
            var favourite: Int = 0

            try {
                val call: Call<ResponseBody>? =
                    WeatherAppClient.client.create(WeatherAppApi::class.java).currentRequest(
                        latitude!!, longitude!!, passKey!!
                    )
                call!!.enqueue(object : Callback<ResponseBody?> {
                    override fun onResponse(
                        call: Call<ResponseBody?>,
                        response: retrofit2.Response<ResponseBody?>
                    ) {
                        val responseBodyString = response.body()!!.string()


                        if (response!!.body().toString().replace("", "") != "") {
                            try {
                                var json = Gson().toJson(responseBodyString) as String
                                json = json.replace("\\\"", "'")
                                json = json.substring(1, json.length - 1)

                                val jsonObject = JSONObject(json)
                                var weatherCurrent = WeatherCurrent()
                                weatherCurrent!!.lon = longitude
                                weatherCurrent!!.lat = latitude
                                weatherCurrent!!.weather_id = jsonObject.optJSONArray("weather")
                                    .getJSONObject(0).optInt("id", 0)
                                weatherCurrent!!.weather_main = jsonObject.optJSONArray("weather")
                                    .getJSONObject(0).optString("main", "")
                                weatherCurrent!!.weather_description =
                                    jsonObject.optJSONArray("weather")
                                        .getJSONObject(0).optString("description", "")
                                weatherCurrent!!.weathericon = jsonObject.optJSONArray("weather")
                                    .getJSONObject(0).optString("icon", "")
                                weatherCurrent!!.base = jsonObject.optString("base", "")
                                weatherCurrent!!.maintemp = jsonObject.optJSONObject("main")
                                    .optDouble("temp", 0.0)
                                weatherCurrent!!.main_feels_like = jsonObject.optJSONObject("main")
                                    .optDouble("feels_like", 0.0)
                                weatherCurrent!!.main_temp_min = jsonObject.optJSONObject("main")
                                    .optDouble("temp_min", 0.0)
                                weatherCurrent!!.main_temp_max = jsonObject.optJSONObject("main")
                                    .optDouble("temp_max", 0.0)
                                weatherCurrent!!.main_pressure = jsonObject.optJSONObject("main")
                                    .optInt("pressure", 0)
                                weatherCurrent!!.main_humidity = jsonObject.optJSONObject("main")
                                    .optInt("humidity", 0)
                                weatherCurrent!!.visibility = jsonObject.optInt("visibility", 0)
                                weatherCurrent!!.wind_speed = jsonObject.optJSONObject("wind")
                                    .optDouble("speed", 0.0)
                                weatherCurrent!!.wind_deg = jsonObject.optJSONObject("wind")
                                    .optInt("deg", 0)
                                weatherCurrent!!.clouds_all = jsonObject.optJSONObject("clouds")
                                    .optInt("all", 0)
                                weatherCurrent!!.dt = jsonObject.optLong("dt", 0)
                                weatherCurrent!!.sys_type = jsonObject.optJSONObject("sys")
                                    .optInt("type", 0)
                                weatherCurrent!!.sys_id = jsonObject.optJSONObject("sys")
                                    .optInt("id", 0)
                                weatherCurrent!!.sys_message = jsonObject.optJSONObject("sys")
                                    .optString("message", "")
                                weatherCurrent!!.sys_country = jsonObject.optJSONObject("sys")
                                    .optString("country", "")
                                weatherCurrent!!.sys_sunrise = jsonObject.optJSONObject("sys")
                                    .optLong("sunrise", 0)
                                weatherCurrent!!.sys_sunset = jsonObject.optJSONObject("sys")
                                    .optLong("sunset", 0)
                                weatherCurrent!!.timezone = jsonObject.optLong("timezone", 0)
                                weatherCurrent!!.id = jsonObject
                                    .optLong("id", 0L)
                                weatherCurrent!!.name = jsonObject
                                    .optString("name", "")
                                weatherCurrent!!.cod = jsonObject.optInt("cod", 0)
                                weatherCurrent.last_updated = getDateTime()
                                weatherCurrent.loc_address = address
                                weatherCurrent.loc_city = city
                                weatherCurrent.loc_state = state
                                weatherCurrent.loc_country = country
                                weatherCurrent.loc_postalCode = postalCode
                                weatherCurrent.loc_knownName = knownName
                                try {
                                    var checkFavourite =
                                        WeatherCurrentDao!!.getAllCompanyWeatherCurrentAll()
                                            ?.filter { a ->
                                                a!!.id == weatherCurrent!!.id
                                            }
                                    if (!checkFavourite.isNullOrEmpty()) {
                                        favourite = checkFavourite!![0]!!.isFavourite!!
                                    }
                                } catch (e: Exception) {

                                }
                                weatherCurrent.isFavourite = favourite
                                weather_current_list.add(weatherCurrent)



                                if (!weather_current_list.isNullOrEmpty()) {
                                    WeatherCurrentDao!!.upsertWeatherCurrentAll(weather_current_list)
                                    weatherForecastRepository!!.getWeatherForecastLive(
                                        weatherCurrent!!.id,
                                        longitude,
                                        latitude,
                                        passKey,
                                        address,
                                        city,
                                        state,
                                        country,
                                        postalCode,
                                        knownName
                                    )
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()

                            }

                        }
                    }

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        Log.v("Verbose", "consolidatedRequest response is ${t.toString()}")
                    }

                })
            } catch (ex: Exception) {
                ex.printStackTrace()
                Log.e("Error", ex.message.toString())
            }

        }
    }

    fun getWeatherCurrentInModel(): LiveData<List<WeatherCurrent?>?>? {


        return WeatherCurrentDao!!.getAllCompanyWeatherCurrent()
    }

    fun getWeatherCurrentInModelFavourites(): LiveData<List<WeatherCurrent?>?>? {


        return WeatherCurrentDao!!.getAllCompanyWeatherFavourites()
    }

    fun getWeatherCurrentInModels(): List<WeatherCurrent?>? {
        return WeatherCurrentDao!!.getAllCompanyWeatherCurrentAll()
    }

    fun updateFavourite(favourite: Int?, weatherCurrent: WeatherCurrent) {
        WeatherCurrentDao!!.updateFavourite(favourite, weatherCurrent.id)
    }

    fun getDateTime(): Long {
        val calendar = Calendar.getInstance()
        return calendar.timeInMillis

    }

    init {
        val db: RoomDatabaseWeatherApp? = RoomDatabaseWeatherApp.getDatabase(application!!)
        WeatherCurrentDao = db!!.weatherCurrentDao()
        weatherForecastRepository = WeatherForecastRepository(application)

        //  allWeatherCurrents=WeatherCurrentDao!!.getAllCompanyWeatherCurrent()
    }
}