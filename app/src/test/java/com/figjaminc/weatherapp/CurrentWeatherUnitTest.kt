package com.weightypremeir.weatherapp

import com.google.android.datatransport.cct.internal.LogResponse.fromJson
import com.weightypremeir.weatherapp.network.WeatherAppApi
import com.weightypremeir.weatherapp.network.WeatherAppClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.protobuf.Parser
import okhttp3.ResponseBody
import org.json.JSONObject
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import retrofit2.Call
import retrofit2.Callback
import java.util.*
import kotlin.concurrent.schedule

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CurrentWeatherUnitTest {

    //intialise
    var latitude: Double? = null
    var longitude: Double? = null
    var aooID: String? = null
    var city: String? = null
    var country: String? = null

    @Before
    fun setup() {

        latitude = -17.7995776
        longitude = 31.0345728
        aooID = "9d2708ee64334d488f63145085eeae12"
        city = "Harare"
        country = "ZW"

    }

    @Test
    fun checkLongitude_isCorrect() {
        var lon: Double? = null


        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).currentRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)

                lon = jsonObject.optJSONObject("coord")
                    .optDouble("lon", 0.0)
            } catch (ex: Exception) {
                ex.printStackTrace()

            }


        }


        assertEquals(String.format("%.4f", longitude).toDouble(), lon)


    }

    //check longitude is correct
    @Test
    fun checkLatitude_isCorrect() {
        var lat: Double? = null

        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).currentRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)

                lat = jsonObject.optJSONObject("coord")
                    .optDouble("lat", 0.0)
            } catch (ex: Exception) {
                ex.printStackTrace()

            }


        }

        assertEquals(String.format("%.4f", latitude).toDouble(), lat)


    }

    //check city name is correct
    @Test
    fun checkCityName_isCorrect() {
        var cityCheck: String? = null
        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).currentRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)

                cityCheck = jsonObject
                    .optString("name", "")
            } catch (ex: Exception) {
                ex.printStackTrace()

            }


        }
        assertEquals(city, cityCheck)


    }

    //check cOUNTRY name is correct
    @Test
    fun checkCountryName_isCorrect() {
        var countryCheck: String? = null
        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).currentRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)


                countryCheck = jsonObject.optJSONObject("sys")
                    .optString("country", "")
            } catch (ex: Exception) {
                ex.printStackTrace()

            }


        }
        assertEquals(country, countryCheck)


    }

}