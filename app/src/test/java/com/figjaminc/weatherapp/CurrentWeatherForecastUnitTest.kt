package com.weightypremeir.weatherapp

import com.weightypremeir.weatherapp.network.WeatherAppApi
import com.weightypremeir.weatherapp.network.WeatherAppClient
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import retrofit2.Call
import retrofit2.Callback
import java.util.*
import kotlin.concurrent.schedule

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CurrentWeatherForecastUnitTest {
    //check longitude is correct
    //intialise
    var latitude: Double? = null
    var longitude: Double? = null
    var aooID: String? = null
    var city: String? = null
    var country: String? = null
    @Before
    fun setup() {

        latitude = -17.7995776
        longitude = 31.0345728
        aooID = "9d2708ee64334d488f63145085eeae12"
        city = "Harare"
        country="ZW"

    }

    @Test
    fun checkLongitude_isCorrect() {
        var lon:Double?=null
              val call: Call<ResponseBody>? =
                WeatherAppClient.client.create(WeatherAppApi::class.java).forecastRequest(
                    latitude!!, longitude!!, aooID!!
                )

            var responseBodyString = call!!.execute()!!.body()!!.string()


            if (responseBodyString.toString() != "") {
                //  responseBodyString = responseBodyString.replace("\\\"", "'")
                var json = Gson().toJson(responseBodyString.toString())


                try {
                    json = json.replace("\\\"", "'")
                    json = json.substring(1, json.length - 1)
                    val jsonObject = JSONObject(json)


                    lon=jsonObject.optJSONObject("city").optJSONObject("coord")
                                    .optDouble("lon",0.0)
                            } catch (ex: Exception) {
                                ex.printStackTrace()

                            }
                        }

            assertEquals( String.format("%.4f", longitude).toDouble(),   lon)


    }

    //check longitude is correct
    @Test
    fun checkLatitude_isCorrect() {
        var lat:Double?=null



            val call: Call<ResponseBody>? =
                WeatherAppClient.client.create(WeatherAppApi::class.java).forecastRequest(
                    latitude!!, longitude!!, aooID!!
                )

            var responseBodyString = call!!.execute()!!.body()!!.string()


            if (responseBodyString.toString() != "") {
                //  responseBodyString = responseBodyString.replace("\\\"", "'")
                var json = Gson().toJson(responseBodyString.toString())


                try {
                    json = json.replace("\\\"", "'")
                    json = json.substring(1, json.length - 1)
                    val jsonObject = JSONObject(json)

                    lat=jsonObject.optJSONObject("city").optJSONObject("coord")
                                    .optDouble("lat",0.0)
                            } catch (ex: Exception) {
                                ex.printStackTrace()

                            }
                        }

               assertEquals( String.format("%.4f", latitude).toDouble(),   lat)


    }

    //check city name is correct
    @Test
    fun checkCityName_isCorrect() {
        var cityCheck:String?=null


        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).forecastRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)

                cityCheck=jsonObject.optJSONObject("city")
                                    .optString("name","")
                            } catch (ex: Exception) {
                                ex.printStackTrace()

                            }
                        }



          assertEquals(city, cityCheck)


    }

    //check cOUNTRY name is correct
    @Test
    fun checkCountryName_isCorrect() {
        var countryCheck:String?=null


        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).forecastRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)

                countryCheck=jsonObject.optJSONObject("city")
                                    .optString("country","")
                            } catch (ex: Exception) {
                                ex.printStackTrace()

                            }
                        }
    assertEquals(country, countryCheck)


    }
    //check city name is correct
    @Test
    fun checkMoreThanOneTemp_isCorrect() {
        var listSize:Int?=0
        var greaterThanOne:Boolean?=null


        val call: Call<ResponseBody>? =
            WeatherAppClient.client.create(WeatherAppApi::class.java).forecastRequest(
                latitude!!, longitude!!, aooID!!
            )

        var responseBodyString = call!!.execute()!!.body()!!.string()


        if (responseBodyString.toString() != "") {
            //  responseBodyString = responseBodyString.replace("\\\"", "'")
            var json = Gson().toJson(responseBodyString.toString())


            try {
                json = json.replace("\\\"", "'")
                json = json.substring(1, json.length - 1)
                val jsonObject = JSONObject(json)

                listSize=jsonObject.optJSONArray("list").length()

                            } catch (ex: Exception) {
                                ex.printStackTrace()

                            }
                        }


        if(listSize!!>=5) {
            greaterThanOne=true
        }
            assertEquals(true, greaterThanOne)


    }
}